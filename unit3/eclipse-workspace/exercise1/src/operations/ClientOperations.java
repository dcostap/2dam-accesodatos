package operations;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.Transaction;

import database.*;
import main.HibernateUtil;

public class ClientOperations {
    private Session session;

    public Client getClient(int idClient) {
        TypedQuery<Client> query1 = session.createQuery("select s from Client s where s.idClient=?1", Client.class);
        query1.setParameter(1, idClient);
        Client client = query1.getSingleResult();
        return client;
    }

    public void addClient(Client client) {
        Transaction tx = session.beginTransaction();
        session.save(client);
        tx.commit();
    }

    public void modifyClient(Client client) {
        Transaction tx = session.beginTransaction();
        TypedQuery<Client> query = session.createQuery("select s from Client s where s.idClient=?1", Client.class);
        query.setParameter(1, client.getIdClient());
        session.update(client);
        tx.commit();
    }

    public void deleteClient(Client client) {
        Transaction tx = session.beginTransaction();

        TypedQuery<Loan> query1 = session.createQuery("select l from Loan l where l.client=?1", Loan.class);
        query1.setParameter(1, client);
        List<Loan> loans = query1.getResultList();
        boolean hasAnyLoans = loans.stream().anyMatch(l -> l.getClient().getIdClient() == client.getIdClient());

        if (!hasAnyLoans)
            session.delete(client);
        tx.commit();
    }

    public List<Client> clientsList() {
        Transaction tx = session.beginTransaction();

        TypedQuery<Client> query1 = session.createQuery("from Client", Client.class);
        List<Client> clients = query1.getResultList();
        tx.commit();
        return clients;
    }

    public ClientOperations(Session session) {
        this.session = session;
    }

}
