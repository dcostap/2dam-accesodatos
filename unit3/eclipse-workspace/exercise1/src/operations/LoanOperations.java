package operations;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.Transaction;

import database.*;
import main.HibernateUtil;

public class LoanOperations {
    private Session session;

    private Book getBook(int bookId) {
        try {
            TypedQuery<Book> query1 = session.createQuery("select s from Book s where s.idBook=?1", Book.class);
            query1.setParameter(1, bookId);
            Book book = query1.getSingleResult();
            return book;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean isBorrowed(int bookId) {
        Transaction tx = session.beginTransaction();

        try {
            Book b = getBook(bookId);
            if (b == null)
                return false;
            TypedQuery<Loan> query1 = session.createQuery("select l from Loan l where l.book=?1 and l.borrowed=true", Loan.class);
            query1.setParameter(1, b);
            Loan loan = query1.getSingleResult();
            return loan != null;
        } catch (NoResultException e) {
            return false;
        } finally {
            tx.commit();
        }
    }

    public String addLoan(int bookId, int clientId) {
        if (isBorrowed(bookId)) {
            return "Book was already borrowed";
        }

        Book book = getBook(bookId);
        if (book == null) {
            return "The book doesn't exist";
        }

        Client client = new ClientOperations(session).getClient(clientId);
        if (client == null) {
            return "The client doesn't exist";
        }

        Loan loan = new Loan(book, client, Date.from(Instant.now()), true);
        Transaction tx = session.beginTransaction();
        session.save(loan);
        tx.commit();

        return "Loan added successfully";
    }

    public String addReturn(int bookId) {
        if (!isBorrowed(bookId)) {
            return "Book is not borrowed";
        }

        Book book = getBook(bookId);
        TypedQuery<Loan> query1 = session.createQuery("select s from Loan s where s.book=?1 and s.borrowed=true", Loan.class);
        query1.setParameter(1, book);
        Loan loan = query1.getSingleResult();

        loan.setBorrowed(false);
        Transaction tx = session.beginTransaction();
        session.update(loan);
        tx.commit();

        return "Book returned successfully";
    }

    public List<Book> borrowedBooksList() {
        Transaction tx = session.beginTransaction();

        TypedQuery<Loan> query1 = session.createQuery("select c from Loan c where c.borrowed=true", Loan.class);
        List<Loan> loan = query1.getResultList();
        tx.commit();

        ArrayList<Book> books = new ArrayList<>();
        loan.forEach(l -> books.add(l.getBook()));
        return books;
    }

    public LoanOperations(Session session) {
        this.session = session;
    }
}
