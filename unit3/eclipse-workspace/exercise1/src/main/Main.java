package main;

import org.hibernate.*;

import database.Book;
import database.Client;
import database.Loan;
import operations.ClientOperations;
import operations.LoanOperations;

import java.util.*;
import javax.persistence.TypedQuery;

public class Main {

	public static void main(String[] args) {
		try {
			System.out.println("Opening connection...");
			Session session = HibernateUtil.openSession();
			ClientOperations co = new ClientOperations(session);

			Scanner sc = new Scanner(System.in);

			System.out.println("Type a client id");
			int id = sc.nextInt();

			Client c = co.getClient(id);
			System.out.println("Name: " + c.getName());
			System.out.print("Loans: ");

			for (Object o : c.getLoans()) {
				Loan l = (Loan) o;
				System.out.print("" + l.getDate() + " (" + (l.getBorrowed() ? "Not" : "") + "Returned); ");
			}
			System.out.println();

			System.out.println("Adding a client...");
			Client newClient = new Client("45957088Y", "Pepito", "pepito@gmail.com", new HashSet<>());
			co.addClient(newClient);

			System.out.println("Clients list...");
			for (Client cc : co.clientsList()) {
				System.out.println("Client: " + cc.getName() + "; dni: " + cc.getDni());
			}

			System.out.println("Modifying the previous client...");
			newClient.setName("Manolito");
			co.modifyClient(newClient);

			System.out.println("Clients list...");
			for (Client cc : co.clientsList()) {
				System.out.println("Client: " + cc.getName() + "; dni: " + cc.getDni());
			}

			System.out.println("Deleting the previous client...");
			co.deleteClient(newClient);

			System.out.println("Clients list...");
			for (Client cc : co.clientsList()) {
				System.out.println("Client: " + cc.getName() + "; dni: " + cc.getDni());
			}

			System.out.println("-------");
			System.out.println("Loan Operations:");
			System.out.println("Adding a loan...");
			LoanOperations lo = new LoanOperations(session);

			System.out.println(lo.addLoan(1, 2));

			System.out.println("List of borrowed books:");
			for (Book b : lo.borrowedBooksList()) {
				System.out.println("Book " + b.getTitle() + ", id: " + b.getIdBook());
			}

			System.out.println("Returning the previous book...");
			System.out.println(lo.addReturn(1));

			System.out.println("List of borrowed books:");
			for (Book b : lo.borrowedBooksList()) {
				System.out.println("Book " + b.getTitle() + ", id: " + b.getIdBook());
			}

			System.out.println("Returning a made-up book...");
			System.out.println(lo.addReturn(555));

			HibernateUtil.closeSession(session);
		}
		catch(Exception erro) {
			erro.printStackTrace();
		}

	}

}
