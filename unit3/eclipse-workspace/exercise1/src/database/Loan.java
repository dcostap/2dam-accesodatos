package database;
// Generated 13:44:48, 09/12/2020 by Hibernate Tools 5.2.12.Final

import java.util.Date;

/**
 * Loan generated by hbm2java
 */
public class Loan implements java.io.Serializable {

	private Integer idLoan;
	private Book book;
	private Client client;
	private Date date;
	private Boolean borrowed;

	public Loan() {
	}

	public Loan(Book book, Client client, Date date, Boolean borrowed) {
		this.book = book;
		this.client = client;
		this.date = date;
		this.borrowed = borrowed;
	}

	public Integer getIdLoan() {
		return this.idLoan;
	}

	public void setIdLoan(Integer idLoan) {
		this.idLoan = idLoan;
	}

	public Book getBook() {
		return this.book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public Client getClient() {
		return this.client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Boolean getBorrowed() {
		return this.borrowed;
	}

	public void setBorrowed(Boolean borrowed) {
		this.borrowed = borrowed;
	}

}
