package database;
// Generated 13:50:11, 03/12/2020 by Hibernate Tools 5.2.12.Final

/**
 * Alumno generated by hbm2java
 */
public class Alumno implements java.io.Serializable {

	private AlumnoId id;

	public Alumno() {
	}

	public Alumno(AlumnoId id) {
		this.id = id;
	}

	public AlumnoId getId() {
		return this.id;
	}

	public void setId(AlumnoId id) {
		this.id = id;
	}

}
