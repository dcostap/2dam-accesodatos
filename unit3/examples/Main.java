import database.*;
import org.hibernate.*;
import java.util.*;
import javax.persistence.TypedQuery;

public class Main {

	public static void main(String[] args) {
		try {
			//Create a session
			SessionFactory sesion = HibernateUtil.getSessionFactory();
			Session session = sesion.openSession();
			
			//Add a student
			/*Transaction tx = session.beginTransaction();
			Student student = new Student("99999999C","Laura","Ferradas",22);
			session.save(student);
			tx.commit();*/
			
			/* //Modify a student
			Transaction tx = session.beginTransaction();
			//Bring the object from the database
			TypedQuery<Student> query2 = session.createQuery("select s from Student s where s.dni=?1", Student.class);
			query2.setParameter(1, "99999999C");
			Student student = query2.getSingleResult();
			//Modify the object
			student.setName("Alba");
			student.setSurname("Agulla");
			student.setAge(30);
			//Make it persistent in the database
			session.update(student);
			tx.commit(); */
			
			//Delete a student
			Transaction tx = session.beginTransaction();
			//Bring the object from the database
			TypedQuery<Student> query2 = session.createQuery("select s from Student s where s.dni=?1", Student.class);
			query2.setParameter(1, "99999999C");
			Student student = query2.getSingleResult();
			//Delete the object from the database
			session.delete(student);
			tx.commit();
			
			//All data from the table student
			System.out.println("LIST OF STUDENTS:");
			TypedQuery<Student> query = session.createQuery("from Student", Student.class);
			List<Student> listado = query.getResultList();
			/*for (Student estudante:listado)
				System.out.println(estudante);*/
			listado.forEach(System.out::println);
			
			//get a specific student (dni and name)
			/*TypedQuery<Student> query1 = session.createQuery("select s from Student s where s.dni=?1 and s.name=?2", Student.class);
			query1.setParameter(1, "22222222B");
			query1.setParameter(2, "Hermione");
			Student student = query1.getSingleResult();
			System.out.println("Student:"+student);*/
			
			//Close session
			session.close();
		}
		catch(Exception erro) {
			System.out.println(erro.getMessage());
			erro.printStackTrace();
		}

	}

}
