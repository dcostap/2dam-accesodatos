import database.*;
import java.util.*;

public class Main {

	public static void main(String[] args) {
		try {
			Operations op = new Operations();
			op.openConexion();
			
			//Add a Student
			Student student = new Student("99999999C","Laura","Ferradas",22);
			//op.addStudent(student);
			
			//Modify a student
			Student studentMod = new Student("99999999C","Pepito","Perez",25);
			//op.modifyStudent(studentMod);
			
			//Delete a student
			op.deleteStudent("99999999C");
			
			//Listado
			List<Student> listado = op.studentsList();
			System.out.println("LIST OF STUDENTS:");
			listado.forEach(System.out::println);
			
			op.closeConexion();
		}
		catch(Exception erro) {
			System.out.println(erro.getMessage());
		}
	}//main
}//class
