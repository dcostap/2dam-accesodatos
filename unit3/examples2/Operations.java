package database;

import org.hibernate.*;
import java.util.*;
import javax.persistence.TypedQuery;

public class Operations {
	private Session session;

	public void openConexion() throws Exception{
		SessionFactory sesion = HibernateUtil.getSessionFactory();
		session = sesion.openSession();
	}
	
	public void closeConexion() throws Exception{
		session.close();
	}
	
	//All data from the table student
	public List<Student> studentsList() throws Exception{
		TypedQuery<Student> query = session.createQuery("from Student", Student.class);
		List<Student> listado = query.getResultList();
		return listado;
	}
	
	//Add a student
	public void addStudent(Student student) throws Exception{
		Transaction tx = session.beginTransaction();
		session.save(student);
		tx.commit();
	}
	
	//Modify a student
	public void modifyStudent(Student student) throws Exception{
		Transaction tx = session.beginTransaction();
		//Bring the object from the database
		TypedQuery<Student> query2 = session.createQuery("select s from Student s where s.dni=?1", Student.class);
		query2.setParameter(1,student.getDni());
		Student studentBD = query2.getSingleResult();
		//Modify the object
		studentBD.setName(student.getName());
		studentBD.setSurname(student.getSurname());
		studentBD.setAge(student.getAge());
		//Make it persistent in the database
		session.update(studentBD);
		tx.commit();
	}
	
	//Delete a student
	public void deleteStudent(String dni) throws Exception{
		Transaction tx = session.beginTransaction();
		//Bring the object from the database
		TypedQuery<Student> query2 = session.createQuery("select s from Student s where s.dni=?1", Student.class);
		query2.setParameter(1,dni);
		Student student = query2.getSingleResult();
		//Delete the object from the database
		session.delete(student);
		tx.commit();	
	}
}//class
