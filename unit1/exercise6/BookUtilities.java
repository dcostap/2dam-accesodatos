import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.XStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class BookUtilities {
	public static void generateBookDat() {
		ArrayList<Book> books = new ArrayList<>();
		ArrayList<String> authors = new ArrayList<>();
		authors.add("author1");
		authors.add("author2");
		books.add(new Book("isbn1", "title1", authors, 1991, true, new Person("person1", "45454511J")));
		books.add(new Book("isbn2", "title2", authors, 1992, true, new Person("person2", "45454522J")));
		books.add(new Book("isbn3", "title3", authors, 1993, true, new Person("person3", "45454533J")));
		books.add(new Book("isbn4", "title4", authors, 1994, true, new Person("person4", "45454544J")));
		books.add(new Book("isbn5", "title5", authors, 1995, false, new Person("person5", "45454555J")));
		books.add(new Book("isbn6", "title6", authors, 1996, false, new Person("person6", "45454566J")));

		try {
			FileOutputStream outputStream = new FileOutputStream("books.dat");
			ObjectOutputStream fileStream = new ObjectOutputStream(outputStream);

			for (Book b : books) {
				fileStream.writeObject(b);
			}

			fileStream.flush();
			outputStream.flush();

			fileStream.close();
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void showBooks() {
		try {
			FileInputStream inputStream = new FileInputStream("books.dat");
			ObjectInputStream objectStream = new ObjectInputStream(inputStream);

			System.out.println("Reading books from books.dat file...");
			Book book = null;
			while (inputStream.available() > 0) {
				book = (Book) objectStream.readObject();
				if (book != null)
					System.out.println(book);
			}

			objectStream.close();
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void generateXmlBookDom() {
		Document doc = XMLUtilities.createEmptyDOM("books");
		try {
			FileInputStream inputStream = new FileInputStream("books.dat");
			ObjectInputStream objectStream = new ObjectInputStream(inputStream);

			System.out.println("Reading books from books.dat file...");
			Book book = null;
			while (inputStream.available() > 0) {
				book = (Book) objectStream.readObject();
				if (book != null) {
					Element bookEl = doc.createElement("book");
					doc.getDocumentElement().appendChild(bookEl);

					Element element;
					element = XMLUtilities.createTextElement("isbn", book.getIsbn(), doc);
					bookEl.appendChild(element);
					element = XMLUtilities.createTextElement("title", book.getTitle(), doc);
					bookEl.appendChild(element);
					element = XMLUtilities.createTextElement("year", String.valueOf(book.getYear()), doc);
					bookEl.appendChild(element);
					element = XMLUtilities.createTextElement("borrowed", String.valueOf(book.isBorrowed()), doc);
					bookEl.appendChild(element);

					Element personEl = doc.createElement("person");
					bookEl.appendChild(personEl);
					personEl.appendChild(XMLUtilities.createTextElement("name", book.getReader().getName(), doc));
					personEl.appendChild(XMLUtilities.createTextElement("dni", book.getReader().getDni(), doc));

					Element authors = doc.createElement("authors");
					bookEl.appendChild(authors);
					for (String author : book.getAuthors()) {
						authors.appendChild(XMLUtilities.createTextElement("author", author, doc));
					}
				}
			}

			Source fonte = new DOMSource(doc);
			Result resultado = new StreamResult(new java.io.File("books.xml"));
			Transformer transformador = TransformerFactory.newInstance().newTransformer();
			transformador.transform(fonte, resultado);

			objectStream.close();
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void generateXmlBookXStream() {
		try {
			FileInputStream inputStream = new FileInputStream("books.dat");
			ObjectInputStream objectStream = new ObjectInputStream(inputStream);

			System.out.println("Reading books from books.dat file...");
			ArrayList<Book> books = new ArrayList<>();
			Book book = null;
			while (inputStream.available() > 0) {
				book = (Book) objectStream.readObject();
				if (book != null)
					books.add(book);
			}

			objectStream.close();
			inputStream.close();

			XStream xstream = new XStream();
			xstream.setMode(XStream.NO_REFERENCES);
			xstream.alias("book", Book.class);
			xstream.alias("books", List.class);
			xstream.toXML(books, new FileOutputStream("booksXStream.xml"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}