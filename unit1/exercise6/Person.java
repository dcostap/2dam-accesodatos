import java.io.Serializable;

public class Person implements Serializable {
	private static final long serialVersionUID = 123423L;
	private String name;
	private String dni;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public Person(String name, String dni) {
		this.name = name;
		this.dni = dni;
	}
}