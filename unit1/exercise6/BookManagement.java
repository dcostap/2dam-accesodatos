import java.io.FileOutputStream;

public class BookManagement {
	public static void main(String[] args) {
		BookUtilities.generateBookDat();
		BookUtilities.showBooks();
		BookUtilities.generateXmlBookDom();
		BookUtilities.generateXmlBookXStream();
	}
}