import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * XMLUtilities
 */
public class XMLUtilities {
	public static Document createEmptyDOM(String rootTagName) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			DOMImplementation implementation = builder.getDOMImplementation();

			// Create a document with empregados as the root element
			Document documento = implementation.createDocument(null, rootTagName, null);
			documento.setXmlVersion("1.0");
			return documento;
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			System.exit(0);
			return null;
		}
	}

	public static Element createTextElement(String tagName, String text, Document doc) {
		Element e = doc.createElement(tagName);
		e.appendChild(doc.createTextNode(text));
		return e;
	}

	public static Document XMLtoDOM(String xmlDocName) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			return builder.parse(new File(xmlDocName));
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
			System.exit(0);
			return null;
		}
	}

	public static void showRootElement(Document doc) {
		System.out.println("Document root element: " + doc.getDocumentElement().getTagName());
	}

	public static void showElementContents(Element element) {
		NodeList children = element.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Element child = (Element) children.item(i);
			System.out.println("Child number " + i);
			System.out.println("  -> tag: " + child.getTagName());
			System.out.println("  -> text: " + child.getTextContent());
		}
	}

	public static void deleteElementsByTag(String tagName, Document doc) {
		deleteChildrenByTag(tagName, doc.getDocumentElement());
	}

	private static void deleteChildrenByTag(String tagName, Node element) {
		NodeList children = element.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			// Element child = (Element) children.item(i);
			// if (child.getTagName().equals(tagName)) {
			// element.removeChild(child);
			// } else {
			// deleteChildrenByTag(tagName, child);
			// }

			Node child = children.item(i);
			if (child.getNodeName().equals(tagName)) {
				element.removeChild(child);
			} else {
				deleteChildrenByTag(tagName, child);
			}
		}
	}

	private static void DOMtoXML(Document doc, String xmlDocName) {
		Source fonte = new DOMSource(doc);
		Result resultado = new StreamResult(new File(xmlDocName));
		Transformer transformador;
		try {
			transformador = TransformerFactory.newInstance().newTransformer();
			transformador.transform(fonte, resultado);
		} catch (TransformerFactoryConfigurationError | TransformerException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}

	public static void main(String[] args) {
		System.out.println("Testing...");
		Document doc = createEmptyDOM("root");
		showRootElement(doc);

		Element e = doc.getDocumentElement();

		e.appendChild(doc.createElement("el1"));
		e.appendChild(doc.createElement("el2"));
		e.appendChild(createTextElement("createdTextElement", "text inside text element", doc));
		showElementContents(e);

		System.out.println("---------------");
		System.out.println("Testing deleting element el1...");
		deleteElementsByTag("el1", doc);
		showElementContents(e);

		System.out.println("---------------");
		System.out.println("Testing loading from xml...");
		Document docFromXml = XMLtoDOM("orders.xml");
		showRootElement(docFromXml);
		showElementContents(docFromXml.getDocumentElement());
	}
}