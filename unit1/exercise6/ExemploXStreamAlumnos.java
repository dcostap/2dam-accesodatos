import java.io.*;
import com.thoughtworks.xstream.XStream;
import java.util.*;

public class ExemploXStreamAlumnos {
  public static void main(String[] args) {
    try {
      // Crear lista de alumnos
      ArrayList<Alumno> listaAlumnos = new ArrayList<Alumno>();
      Alumno unAlumno = new Alumno("11111111A", "Laura");
      Alumno outroAlumno = new Alumno("22222222B", "Pepe");
      listaAlumnos.add(unAlumno);
      listaAlumnos.add(outroAlumno);

      // Xerar o XML coa informacion que temos na lista
      XStream xstream = new XStream();
      // Para que poña todos os elementos, ainda que estén repetidos, e n
      // n referencias
      xstream.setMode(XStream.NO_REFERENCES);
      // Renomeo Alumno por alumno
      xstream.alias("alumno", Alumno.class);
      // Renomeo list por alumnos
      xstream.alias("alumnos", List.class);
      xstream.toXML(listaAlumnos, new FileOutputStream("alumnosXStream.xml"));
    } catch (Exception erro) {
      erro.printStackTrace();
    }
  }// main

}// class
