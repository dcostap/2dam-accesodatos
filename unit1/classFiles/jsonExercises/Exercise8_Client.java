import java.io.Serializable;

public class Exercise8_Client implements Serializable {
	private String DNI;
	private String name;
	private int price;

	public Exercise8_Client(String DNI, String name, int price) {
		this.DNI = DNI;
		this.name = name;
		this.price = price;
	}

	public String getDNI() {
		return DNI;
	}

	public String getName() {
		return name;
	}

	public int getPrice() {
		return price;
	}

	public String toString() {
		return "Exercise8_Client [DNI=" + DNI + ", name=" + name + ", price=" + price + "]";
	}
}
