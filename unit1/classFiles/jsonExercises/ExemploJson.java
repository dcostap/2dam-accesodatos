import org.json.simple.*;
import org.json.simple.parser.*;
import java.io.*;

public class ExemploJson {
	public static void main(String args[]) {
		createJSON();
		readJSON();

	}// main
		// Method that creates a JSON file

	private static void createJSON() {
		try {
			// JSON Object Creation
			JSONObject obj = new JSONObject();
			obj.put("name", "Luisa");
			obj.put("salary", new Double(600.5));
			obj.put("age", new Integer(20));

			// Adding the array
			JSONArray arr = new JSONArray();
			arr.add("texto array");
			arr.add(new Integer(20));
			arr.add(new Double(600000));

			obj.put("arrai", arr);

			// Address
			JSONObject objAddress = new JSONObject();
			objAddress.put("street", "Rua Nova");
			objAddress.put("number", new Integer(26));

			obj.put("address", objAddress);

			// Save into a file
			FileWriter ficheiro = new FileWriter("example.json");
			ficheiro.write(obj.toJSONString());
			ficheiro.close();
		} catch (Exception erro) {
			System.out.println("createJSON: " + erro.getMessage());
		}
	}

	// Method that reads from a JSON file
	public static void readJSON() {
		try (FileReader ficheiro = new FileReader("example.json")) {
			JSONParser parser = new JSONParser();
			JSONObject obxDocumento = (JSONObject) parser.parse(ficheiro);
			String nome = (String) obxDocumento.get("name");
			double salario = (double) obxDocumento.get("salary");
			long idade = (long) obxDocumento.get("age");
			System.out.println("Nome: " + nome + ", salario: " + salario + ", idade:" + idade);

			JSONArray arrai = (JSONArray) obxDocumento.get("arrai");
			System.out.println("Arrai: " + arrai);
			// Other way to show the contents of the array
			System.out.println("ARRAY: ");
			for (Object elemento : arrai) {
				System.out.println(elemento + ", ");
			}

			// ADDRESS
			JSONObject obxAddress = (JSONObject) obxDocumento.get("address");
			String street = (String) obxAddress.get("street");
			long number = (long) obxAddress.get("number");
			System.out.println("ADDRESS: " + street + " - " + number);
		} catch (Exception erro) {
			System.out.println("readJSON: " + erro.getMessage());
		}

	}// readJSON
}// class