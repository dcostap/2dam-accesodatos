import java.io.FileWriter;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Exercise5
 */
public class Exercise5 {
	public static void main(String[] args) {
		try {
			JSONObject obj = new JSONObject();
			JSONObject menu = new JSONObject();
			obj.put("menu", menu);
			menu.put("id", "file");
			menu.put("value", "File");

			JSONObject popup = new JSONObject();
			menu.put("popup", popup);

			JSONObject menuitem1 = new JSONObject();
			menuitem1.put("value", "New");
			menuitem1.put("onclick", "CreateNewDoc()");

			JSONObject menuitem2 = new JSONObject();
			menuitem2.put("value", "Open");
			menuitem2.put("onclick", "OpenDoc()");

			JSONObject menuitem3 = new JSONObject();
			menuitem3.put("value", "Close");
			menuitem3.put("onclick", "CloseDoc()");

			JSONArray arr = new JSONArray();
			arr.add(menuitem1);
			arr.add(menuitem2);
			arr.add(menuitem3);

			popup.put("menuitems", arr);

			FileWriter ficheiro = new FileWriter("exercise5.json");
			ficheiro.write(obj.toJSONString());
			ficheiro.close();
		} catch (Exception error) {
			error.printStackTrace();
		}
	}
}