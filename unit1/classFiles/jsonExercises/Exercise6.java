import java.io.FileReader;
import java.io.FileWriter;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * Exercise5
 */
public class Exercise6 {
	public static void main(String[] args) {
		try (FileReader ficheiro = new FileReader("exercise5.json")) {
			JSONParser parser = new JSONParser();
			JSONObject doc = (JSONObject) parser.parse(ficheiro);

			JSONObject menu = (JSONObject) doc.get("menu");

			String id = (String) menu.get("id");
			String value = (String) menu.get("value");
			System.out.println("menu: id = " + id + "; value = " + value);

			JSONObject popup = (JSONObject) menu.get("popup");

			JSONArray menuitems = (JSONArray) popup.get("menuitems");
			for (Object object : menuitems) {
				JSONObject menuitem = (JSONObject) object;
				System.out.println("popup: " + menuitem);
				System.out.println("  value = " + menuitem.get("value"));
				System.out.println("  onclick = " + menuitem.get("onclick"));
			}

		} catch (Exception erro) {
			System.out.println("readJSON: " + erro.getMessage());
		}
	}
}