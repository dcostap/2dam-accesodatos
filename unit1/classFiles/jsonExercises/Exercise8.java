import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Exercise5
 */
public class Exercise8 {
	public static void main(String[] args) {
		readExercise7JSON();
		readOrdersDat();
	}

	private static void readExercise7JSON() {
		try (FileReader ficheiro = new FileReader("exercise7.json")) {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			DOMImplementation implementation = builder.getDOMImplementation();

			Document document = implementation.createDocument(null, "orders", null);
			document.setXmlVersion("1.0");

			// Element elementOrder = documento.createElement("order");
			// documento.getDocumentElement().appendChild(elementOrder);

			// Element id = documento.createElement("idOrder");
			// id.setTextContent(String.valueOf(order.getIdOrder()));
			// elementOrder.appendChild(id);

			List<Exercise8_Order> ordersObjects = new ArrayList<>();

			JSONParser parser = new JSONParser();
			JSONObject doc = (JSONObject) parser.parse(ficheiro);

			JSONArray orders = (JSONArray) doc.get("orders");
			for (Object object : orders) {
				JSONObject order = (JSONObject) object;

				Exercise8_Order orderObject = new Exercise8_Order();
				ordersObjects.add(orderObject);

				Element orderXml = document.createElement("order");
				document.getDocumentElement().appendChild(orderXml);

				Element orderId = document.createElement("id");
				orderId.setTextContent(order.get("id").toString());
				orderXml.appendChild(orderId);

				Element orderPrice = document.createElement("price");
				orderPrice.setTextContent(order.get("price").toString());
				orderXml.appendChild(orderPrice);

				Element orderDelivered = document.createElement("delivered");
				orderDelivered.setTextContent(order.get("delivered").toString());
				orderXml.appendChild(orderDelivered);

				System.out.println("Order... ");

				System.out.println("  id = " + order.get("id"));
				System.out.println("  price = " + order.get("price"));
				System.out.println("  delivered = " + order.get("delivered"));

				orderObject.setId(Integer.parseInt(order.get("id").toString()));
				orderObject.setPrice(Integer.parseInt(order.get("price").toString()));
				orderObject.setDelivered(Boolean.parseBoolean(order.get("delivered").toString()));

				System.out.println("Client of order... ");
				JSONObject client = (JSONObject) order.get("client");
				System.out.println("  DNI = " + client.get("DNI"));
				System.out.println("  name = " + client.get("name"));
				System.out.println("  price = " + client.get("price"));

				// object client
				Exercise8_Client clientObject = new Exercise8_Client(client.get("DNI").toString(),
						client.get("name").toString(), Integer.parseInt(client.get("price").toString()));
				orderObject.setClient(clientObject);

				// xml client
				Element clientXml = document.createElement("client");
				orderXml.appendChild(clientXml);

				Element clientDNI = document.createElement("DNI");
				clientDNI.setTextContent(client.get("DNI").toString());
				clientXml.appendChild(clientDNI);

				Element clientName = document.createElement("name");
				clientName.setTextContent(client.get("name").toString());
				clientXml.appendChild(clientName);

				Element clientPrice = document.createElement("price");
				clientPrice.setTextContent(client.get("price").toString());
				clientXml.appendChild(clientPrice);

				orderDelivered.setTextContent(order.get("delivered").toString());

				Element orderRowsXml = document.createElement("orderRows");
				orderXml.appendChild(orderRowsXml);

				JSONArray orderRows = (JSONArray) order.get("orderRows");
				System.out.println("Array of orderRow...");

				List<Exercise8_OrderRow> orderRowsArray = new ArrayList<>();
				orderObject.setOrderRows(orderRowsArray);
				for (Object orderRowObject : orderRows) {
					JSONObject orderRow = (JSONObject) orderRowObject;
					System.out.println("OrderRow...");
					System.out.println("  amount = " + orderRow.get("amount"));
					System.out.println("  price = " + orderRow.get("price"));

					Element orderRowXml = document.createElement("orderRow");
					orderRowsXml.appendChild(orderRowXml);

					Element orderRowPrice = document.createElement("price");
					orderRowPrice.setTextContent(orderRow.get("price").toString());
					orderRowXml.appendChild(orderRowPrice);

					Element orderRowAmount = document.createElement("amount");
					orderRowAmount.setTextContent(orderRow.get("amount").toString());
					orderRowXml.appendChild(orderRowAmount);

					Element orderRowProduct = document.createElement("product");
					orderRowXml.appendChild(orderRowProduct);

					JSONObject product = (JSONObject) orderRow.get("product");
					System.out.println("OrderRow product...");
					System.out.println("  id = " + product.get("id"));
					System.out.println("  productName = " + product.get("productName"));
					System.out.println("  description = " + product.get("description"));
					System.out.println("  picture = " + product.get("picture"));
					System.out.println("  price = " + product.get("price"));

					// xml
					Element orderRowProductPrice = document.createElement("price");
					orderRowProductPrice.setTextContent(product.get("price").toString());
					orderRowProduct.appendChild(orderRowProductPrice);

					Element orderRowProductId = document.createElement("id");
					orderRowProductId.setTextContent(product.get("id").toString());
					orderRowProduct.appendChild(orderRowProductId);

					Element orderRowProductProductName = document.createElement("productName");
					orderRowProductProductName.setTextContent(product.get("productName").toString());
					orderRowProduct.appendChild(orderRowProductProductName);

					Element orderRowProductDescription = document.createElement("description");
					orderRowProductDescription.setTextContent(product.get("description").toString());
					orderRowProduct.appendChild(orderRowProductDescription);

					Element orderRowProductPicture = document.createElement("picture");
					orderRowProductPicture.setTextContent(product.get("picture").toString());
					orderRowProduct.appendChild(orderRowProductPicture);

					// object
					Exercise8_OrderRow orderRowInstance = new Exercise8_OrderRow();
					orderRowsArray.add(orderRowInstance);

					orderRowInstance.setAmount(Integer.parseInt(orderRow.get("amount").toString()));
					orderRowInstance.setPrice(Integer.parseInt(orderRow.get("price").toString()));

					Exercise8_Product productInstance = new Exercise8_Product();
					orderRowInstance.setProduct(productInstance);

					productInstance.setDescription(product.get("description").toString());
					productInstance.setPicture(product.get("picture").toString());
					productInstance.setId(Integer.parseInt(product.get("id").toString()));
					productInstance.setPrice(Integer.parseInt(product.get("price").toString()));
					productInstance.setProductName(product.get("productName").toString());
				}
			}

			// serialize objects
			FileOutputStream outputStream = new FileOutputStream("orders.dat");
			ObjectOutputStream fileStream = new ObjectOutputStream(outputStream);

			for (Exercise8_Order o : ordersObjects) {
				fileStream.writeObject(o);
			}

			fileStream.flush();
			outputStream.flush();

			fileStream.close();
			outputStream.close();

			// generate xml
			Source fonte = new DOMSource(document);
			Result resultado = new StreamResult(new java.io.File("exercise8.xml"));
			Transformer transformador = TransformerFactory.newInstance().newTransformer();
			transformador.transform(fonte, resultado);
		} catch (Exception error) {
			error.printStackTrace();
		}
	}

	private static void readOrdersDat() {
		try {
			FileInputStream inputStream = new FileInputStream("orders.dat");
			ObjectInputStream objectStream = new ObjectInputStream(inputStream);

			System.out.println("Reading orders from orders.dat file...");
			Exercise8_Order order = null;
			while (inputStream.available() > 0) {
				order = (Exercise8_Order) objectStream.readObject();
				if (order != null)
					System.out.println(order);
			}

			objectStream.close();
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}