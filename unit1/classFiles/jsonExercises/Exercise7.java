import java.io.FileWriter;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Exercise7 {
	public static void main(String[] args) {
		try {
			JSONObject obj = new JSONObject();
			JSONArray orders = new JSONArray();
			obj.put("orders", orders);

			JSONObject order1 = new JSONObject();
			orders.add(order1);

			order1.put("id", 1932);

			JSONObject client1 = new JSONObject();
			client1.put("DNI", "45958555J");
			client1.put("name", "Pepe");
			client1.put("price", 100);

			order1.put("client", client1);

			JSONArray orderRows = new JSONArray();
			order1.put("orderRows", orderRows);

			JSONObject orderRow1 = new JSONObject();
			orderRows.add(orderRow1);

			JSONObject product1 = new JSONObject();
			product1.put("id", 12333);
			product1.put("productName", "Name of product");
			product1.put("description", "Description of product");
			product1.put("picture", "picture.jpg");
			product1.put("price", 10);

			orderRow1.put("product", product1);
			orderRow1.put("amount", 5);
			orderRow1.put("price", 50);

			JSONObject orderRow2 = new JSONObject();
			orderRows.add(orderRow2);

			JSONObject product2 = new JSONObject();
			product2.put("id", 12938);
			product2.put("productName", "Name of product 2");
			product2.put("description", "Description of product 2");
			product2.put("picture", "picture2.jpg");
			product2.put("price", 800);

			orderRow2.put("product", product2);
			orderRow2.put("amount", 1);
			orderRow2.put("price", 800);

			order1.put("price", 1000);
			order1.put("delivered", true);

			FileWriter ficheiro = new FileWriter("exercise7.json");
			ficheiro.write(obj.toJSONString());
			ficheiro.close();
		} catch (Exception error) {
			error.printStackTrace();
		}
	}
}
