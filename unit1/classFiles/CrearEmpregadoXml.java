import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
public class CrearEmpregadoXml{
  public static void main(String[] args){
    //Arrais cos datos que se incluiran no XML
    String[] apelido = {"Fernandez","Gil","Lopez","Ramos","Sevilla"};
    int[] dep = {10,15,5,20,25};
    double[] salario = {1000.5,1200,800,1234.56,2000};
    try{
      //Crear documento DOM na memoria
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      DOMImplementation implementation = builder.getDOMImplementation();
     //Obtemos Document co elemento "empregados" como raiz
      Document documento = implementation.createDocument(null,"empregados",null);
      documento.setXmlVersion("1.0");

     //Creamos os elementos <empregado> que van dentro do raiz <empregados>
      for(int i=0;i<apelido.length;i++){//Recorremos os arrais
        Element elEmpregado = documento.createElement("empregado");//Creo elemento
        documento.getDocumentElement().appendChild(elEmpregado); //Engado no raiz
        //Creo o elemento id
        Element elId = documento.createElement("id");
        elEmpregado.appendChild(elId);
        //Creo o texto que vai dentro de <id>
        Text textId = documento.createTextNode((i+1)+"");
        elId.appendChild(textId);
        //Creo o elemento apelido
        Element elApelido = documento.createElement("apelido");
        elEmpregado.appendChild(elApelido);
        //Creo o texto que vai dentro de <apelido>
        Text textApelido = documento.createTextNode(apelido[i]);
        elApelido.appendChild(textApelido);
        //Creo o elemento dep
        Element elDep = documento.createElement("dep");
        elEmpregado.appendChild(elDep);
        //Creo o texto que vai dentro de <dep>
        Text textDep = documento.createTextNode(dep[i]+"");
        elDep.appendChild(textDep);
        //Creo o elemento salario
        Element elSalario = documento.createElement("salario");
        elEmpregado.appendChild(elSalario);
        //Creo o texto que vai dentro de <salario>
        Text textSalario = documento.createTextNode(salario[i]+"");
        elSalario.appendChild(textSalario);

      }//for
   
      //==================================
      //Creamos a fonte XML a partir do documento en memoria
      Source fonte = new DOMSource(documento);
      //Ponemos o resultado en no ficheiro empregados.xml
      Result resultado = new StreamResult(new java.io.File("empregados.xml"));
      //Transformar o documento fonte o ficheiro no disco duro
      Transformer transformador = TransformerFactory.newInstance().newTransformer();
      transformador.transform(fonte,resultado);
 
    }//try  
    catch(Exception erro){
      erro.printStackTrace();
    }//catch
  }//main
}//class 
