package orders;

import java.io.Serializable;

public class Product implements Serializable {
	private int idProduct;
	private String description;
	private double price;

	public Product(int idProduct, String description, double price) {
		this.idProduct = idProduct;
		this.description = description;
		this.price = price;
	}

	public int getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}