import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

public class CreateDiary {
	public static void main(String[] args) {
		String[] names = new String[] { "Pepe", "Luis", "Iván", "Abel", "Manuel", "Jaime" };
		String[] telephones = new String[] { "609331150", "609335555", "609333333", "454545454", "535384858",
				"123123534" };
		String[] streets = new String[] { "avenida 1", "avenida 2", "avenida 3", "avenida 4", "avenida 5",
				"avenida 6" };

		String[] streetNumbers = new String[] { "3", "2", "11", "7", "1", "99" };

		Document documento = XMLUtilities.createEmptyDOM("diary");
		try {
			for (int i = 0; i < names.length; i++) {
				Element elementContact = documento.createElement("contact");
				elementContact.setAttribute("id", Integer.toString(i));

				documento.getDocumentElement().appendChild(elementContact);

				Element name = documento.createElement("name");
				name.setTextContent(names[i]);
				elementContact.appendChild(name);

				Element telephone = documento.createElement("telephone");
				telephone.setTextContent(telephones[i]);
				elementContact.appendChild(telephone);

				Element address = documento.createElement("address");
				elementContact.appendChild(address);

				Element street = documento.createElement("street");
				street.setTextContent(streets[i]);
				address.appendChild(street);

				Element streetNo = documento.createElement("number");
				streetNo.setTextContent(streetNumbers[i]);
				address.appendChild(streetNo);

				XMLUtilities.DOMtoXML(documento, "diary.xml");
			}
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		}
	}
}
