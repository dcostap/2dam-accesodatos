import org.w3c.dom.*;

import java.io.File;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

public class ProcessDiary {
	public static void main(String[] args) {
		try {
			Document doc = XMLUtilities.XMLtoDOM("diary.xml");
			System.out.println("Root element:");
			XMLUtilities.showRootElement(doc);
			System.out.println("Contents of diary.xml:");
			XMLUtilities.showElementContents(doc.getDocumentElement());

			XMLUtilities.deleteElementsByTag("telephone", doc);
			XMLUtilities.DOMtoXML(doc, "diary1.xml");
			XMLUtilities.deleteElementsByTag("address", doc);
			XMLUtilities.DOMtoXML(doc, "diary2.xml");

			NodeList contacts = doc.getElementsByTagName("contact");
			for (int i = 0; i < contacts.getLength(); i++) {
				Element e = XMLUtilities.createTextElement("email", "email text", doc);
				Node contact = contacts.item(i);
				contact.appendChild(e);
			}

			XMLUtilities.DOMtoXML(doc, "diary3.xml");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
