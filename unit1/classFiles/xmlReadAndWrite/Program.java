import orders.*;

public class Program {
	public static void main(String[] args) {
		CreateXmlOrders xml = new CreateXmlOrders();

		xml.createOrdersXml(xml.createOrdersList());
		xml.readOrdersXml();
	}
}
