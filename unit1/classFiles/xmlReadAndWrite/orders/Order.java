package orders;

import java.util.ArrayList;

public class Order {
	private int idOrder;
	private String clientName;
	private ArrayList<Product> products;

	public Order(int idOrder, String clientName, ArrayList<Product> products) {
		this.idOrder = idOrder;
		this.clientName = clientName;
		this.products = products;
	}

	public int getIdOrder() {
		return idOrder;
	}

	public void setIdOrder(int idOrder) {
		this.idOrder = idOrder;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public ArrayList<Product> getProducts() {
		return products;
	}

	public void setProducts(ArrayList<Product> products) {
		this.products = products;
	}

	@Override
	public String toString() {
		return "Order [clientName=" + clientName + ", idOrder=" + idOrder + ", products=" + products + "]";
	}

}