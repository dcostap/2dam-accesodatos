package orders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

public class CreateXmlOrders {
	public ArrayList<Order> createOrdersList() {
		ArrayList<Order> orders = new ArrayList<>();
		ArrayList<Product> productos = new ArrayList<>();
		productos.add(new Product(0, "descriptionProduct0", 10.0));
		orders.add(new Order(0, "client0", productos));

		ArrayList<Product> productos2 = new ArrayList<>();
		productos2.add(new Product(1, "descriptionProduct1", 550.0));
		orders.add(new Order(1, "client1", productos2));

		return orders;
	}

	public void storeOrders(ArrayList<Order> orders) {
		try {
			FileOutputStream outputStream = new FileOutputStream("orders.dat");
			ObjectOutputStream fileStream = new ObjectOutputStream(outputStream);

			for (Order o : orders) {
				fileStream.writeObject(o);
			}

			fileStream.flush();
			outputStream.flush();

			fileStream.close();
			outputStream.close();
		} catch (IOException erro) {
			System.out.println("Erro E/S " + erro.getMessage());
		} catch (Exception erro) {
			System.out.println("Outro erro " + erro.getMessage());
		}
	}

	public void readOrdersFile() {
		try {
			FileInputStream inputStream = new FileInputStream("orders.dat");
			ObjectInputStream objectStream = new ObjectInputStream(inputStream);

			System.out.println("Reading orders from orders.dat file...");
			Order order = null;
			while (inputStream.available() > 0) {
				order = (Order) objectStream.readObject();
				if (order != null)
					System.out.println(order);
			}

			objectStream.close();
			inputStream.close();
		} catch (IOException erro) {
			System.out.println("I/O error " + erro.getMessage());
		} catch (Exception erro) {
			System.out.println("Unidentified error " + erro.getMessage());
		}
	}

	public ArrayList<Order> readOrders() {
		ArrayList<Order> orders = new ArrayList<>();
		try {
			FileInputStream inputStream = new FileInputStream("orders.dat");
			ObjectInputStream objectStream = new ObjectInputStream(inputStream);

			System.out.println("Reading orders from orders.dat file...");
			Order order = null;
			while (inputStream.available() > 0) {
				order = (Order) objectStream.readObject();
				if (order != null)
					orders.add(order);
			}

			objectStream.close();
			inputStream.close();
		} catch (IOException erro) {
			System.out.println("I/O error " + erro.getMessage());
		} catch (Exception erro) {
			System.out.println("Unidentified error " + erro.getMessage());
		}

		return orders;
	}

	public void createOrdersXml(ArrayList<Order> orders) {
		try {
			// Create an empty DOM document in the memory
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			DOMImplementation implementation = builder.getDOMImplementation();

			Document documento = implementation.createDocument(null, "orders", null);
			documento.setXmlVersion("1.0");

			for (Order order : orders) {
				Element elementOrder = documento.createElement("order");
				documento.getDocumentElement().appendChild(elementOrder);

				Element id = documento.createElement("idOrder");
				id.setTextContent(String.valueOf(order.getIdOrder()));
				elementOrder.appendChild(id);

				Element clientName = documento.createElement("clientName");
				clientName.setTextContent(String.valueOf(order.getClientName()));
				elementOrder.appendChild(clientName);

				Element products = documento.createElement("products");
				elementOrder.appendChild(products);

				for (Product p : order.getProducts()) {
					Element elementProduct = documento.createElement("product");
					products.appendChild(elementProduct);

					Element idProduct = documento.createElement("idProduct");
					idProduct.setTextContent(String.valueOf(p.getIdProduct()));
					products.appendChild(idProduct);

					Element desc = documento.createElement("description");
					desc.setTextContent(String.valueOf(p.getDescription()));
					products.appendChild(desc);

					Element price = documento.createElement("price");
					price.setTextContent(String.valueOf(p.getPrice()));
					products.appendChild(price);
				}
			}

			// Creamos a fonte XML a partir do documento en memoria
			Source fonte = new DOMSource(documento);
			// Ponemos o resultado en no ficheiro empregados.xml
			Result resultado = new StreamResult(new java.io.File("orders.xml"));
			// Transformar o documento fonte o ficheiro no disco duro
			Transformer transformador = TransformerFactory.newInstance().newTransformer();
			transformador.transform(fonte, resultado);

		} catch (Exception erro) {
			erro.printStackTrace();
		}
	}

	public void readOrdersXml() {
		try {
			// Cargamos o documento na memoria usando DOM
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document documento = builder.parse(new File("orders.xml"));
			documento.getDocumentElement().normalize();

			NodeList orders = documento.getElementsByTagName("orders");
			for (int i = 0; i < orders.getLength(); i++) {
				Node nodoOrder = orders.item(i);
				Element order = (Element) nodoOrder;

				System.out.println("idOrder: " + order.getElementsByTagName("idOrder").item(0).getTextContent());
				System.out.println("clientName: " + order.getElementsByTagName("clientName").item(0).getTextContent());

				NodeList products = order.getElementsByTagName("products");
				for (int j = 0; j < products.getLength(); j++) {
					Node nodoProduct = products.item(j);
					Element product = (Element) nodoProduct;
					System.out.println(
							"idProduct: " + product.getElementsByTagName("idProduct").item(0).getTextContent());
					System.out.println(
							"description: " + product.getElementsByTagName("description").item(0).getTextContent());
					System.out.println("price: " + product.getElementsByTagName("price").item(0).getTextContent());
				}
			}
		} catch (Exception erro) {
			System.out.println("Erro na lectura de empregados.xml: " + erro.getMessage());
		}
	}
}