import java.io.Serializable;
public class Alumno implements Serializable{
  private String dni;
  private String nome;
  final static long serialVersionUID=1L;
  public Alumno(String dni,String nome){
    this.dni=dni;
    this.nome=nome;
  }
  public String toString(){
    return dni + " - " + nome;
  }
}//class
