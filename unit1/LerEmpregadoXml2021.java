import java.io.File;
import javax.xml.parsers.*;
import org.w3c.dom.*;

public class LerEmpregadoXml2021{
  public static void main(String[] args){
    try{
      //Cargamos o documento na memoria usando DOM
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      Document documento = builder.parse(new File("empregados.xml"));
      documento.getDocumentElement().normalize();

      NodeList empregados = documento.getElementsByTagName("empregado");
      for(int i=0; i<empregados.getLength(); i++){
        Node nodoEmpregado = empregados.item(i);
        Element elEmpregado = (Element) nodoEmpregado;
        //id
        Element elId = (Element)elEmpregado.getElementsByTagName("id").item(0);
        System.out.println("ID: "+elId.getTextContent());
      }//for
    }//try
    catch(Exception erro){
      System.out.println("Erro na lectura de empregados.xml: "+erro.getMessage());
    }

  }//main
}//class
