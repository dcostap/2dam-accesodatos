import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

public class CreateDiary {
	public static void main(String[] args) {
		String[] names = new String[] { "Pepe", "Luis", "Iván", "Abel", "Manuel", "Jaime" };
		String[] telephones = new String[] { "609331150", "609335555", "609333333", "454545454", "535384858",
				"123123534" };
		String[] streets = new String[] { "avenida 1", "avenida 2", "avenida 3", "avenida 4", "avenida 5",
				"avenida 6" };

		String[] streetNumbers = new String[] { "3", "2", "11", "7", "1", "99" };

		// Create an empty DOM document in the memory
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			DOMImplementation implementation = builder.getDOMImplementation();

			Document documento = implementation.createDocument(null, "diary", null);
			documento.setXmlVersion("1.0");

			for (int i = 0; i < names.length; i++) {
				Element elementContact = documento.createElement("contact");
				elementContact.setAttribute("id", Integer.toString(i));

				documento.getDocumentElement().appendChild(elementContact);

				Element name = documento.createElement("name");
				name.setTextContent(names[i]);
				elementContact.appendChild(name);

				Element telephone = documento.createElement("name");
				telephone.setTextContent(telephones[i]);
				elementContact.appendChild(telephone);

				Element address = documento.createElement("addres");
				elementContact.appendChild(address);

				Element street = documento.createElement("street");
				street.setTextContent(streets[i]);
				address.appendChild(street);

				Element streetNo = documento.createElement("street");
				streetNo.setTextContent(streets[i]);
				address.appendChild(streetNo);
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
}
