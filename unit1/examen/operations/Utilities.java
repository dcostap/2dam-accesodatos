package operations;

import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import java.io.*;
import java.util.*;

public class Utilities {
	public static ArrayList<Team> teamsToArrai() {
		try {
			// Read the xml file into a DOM document
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();

			Document document = builder.parse("tournament.xml");
			document.getDocumentElement().normalize();

			ArrayList<Team> teams = new ArrayList<>();

			// Start reading all the tags that represent the teams
			NodeList teamNodes = document.getElementsByTagName("team");

			for (int i = 0; i < teamNodes.getLength(); i++) {
				Element teamEl = (Element) teamNodes.item(i);
				Team team = new Team();
				team.name = teamEl.getElementsByTagName("name").item(0).getTextContent();
				team.city = teamEl.getElementsByTagName("city").item(0).getTextContent();

				// Get the value from the attribute named "code"
				for (int a = 0; a < teamEl.getAttributes().getLength(); a++) {
					Node attribute = teamEl.getAttributes().item(a);
					if (attribute.getNodeName().equals("code")) {
						team.code = Integer.parseInt(attribute.getNodeValue());
						break;
					}
				}



				List<Member> members = new ArrayList<Member>();
				team.members = members;
				
				// Find all the member elements
				Element membersEl = (Element) teamEl.getElementsByTagName("members").item(0);
				NodeList memberNodes = membersEl.getElementsByTagName("member");

				for (int n = 0; n < memberNodes.getLength(); n++) {
					Element memberEl = (Element) memberNodes.item(n);
					Member member = new Member();
					member.name = memberEl.getElementsByTagName("name").item(0).getTextContent();
					member.age = Integer.parseInt(memberEl.getElementsByTagName("age").item(0).getTextContent());
					member.position = memberEl.getElementsByTagName("position").item(0).getTextContent();

					members.add(member);
				}

				teams.add(team);
			}

			return teams;
		} catch (ParserConfigurationException exception) {
			System.out.println("There was an error parsing the xml file. A ParserConfigurationException was thrown");			
			exception.printStackTrace();
			return null;
		} catch (Exception exception) {
			System.out.println("An unidentified exception was thrown");			
			exception.printStackTrace();
			return null;
		}
	}

	public static void arraiToJSON(ArrayList<Team> teams) {
		try {
			// Create an empty JSON object
			JSONObject jsonRoot = new JSONObject();
			JSONObject tournamentJson = new JSONObject();
			jsonRoot.put("tournament", tournamentJson);
			
			tournamentJson.put("id", 1);
			JSONArray participantsArrayJson = new JSONArray();
			tournamentJson.put("participants", participantsArrayJson);

			// Add all the information about every team stored in the ArrayList
			for (Team team : teams) {						
				JSONObject teamJsonRoot = new JSONObject();
				participantsArrayJson.add(teamJsonRoot);

				JSONObject teamJson = new JSONObject();
				teamJsonRoot.put("team", teamJson);

				teamJson.put("code", team.code);
				teamJson.put("name", team.name);
				teamJson.put("city", team.city);

				JSONArray membersArrayJson = new JSONArray();
				teamJson.put("members", membersArrayJson);

				for (Member member : team.members) {						
					JSONObject memberJsonRoot = new JSONObject();
					membersArrayJson.add(memberJsonRoot);

					JSONObject memberJson = new JSONObject();
					memberJsonRoot.put("member", memberJson);
					
					memberJson.put("name", member.name);
					memberJson.put("age", member.age);
					memberJson.put("position", member.position);
				}
			}


			// Write the JSON object stored in memory into the disk
			FileWriter file = new FileWriter("tournaments.json");
			file.write(jsonRoot.toJSONString());
			file.flush();
			file.close();
		} catch (IOException exception) {
			System.out.println("There was an error writing the JSON file. An Input / Output exception was thrown");			
			exception.printStackTrace();
		} catch (Exception exception) {
			System.out.println("An unidentified exception was thrown");			
			exception.printStackTrace();
		}
	}
}
