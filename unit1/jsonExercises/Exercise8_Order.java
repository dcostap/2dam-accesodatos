import java.io.Serializable;
import java.util.List;

public class Exercise8_Order implements Serializable {
	private int id;
	private int price;
	private boolean delivered;
	private Exercise8_Client client;
	private List<Exercise8_OrderRow> orderRows;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public boolean isDelivered() {
		return delivered;
	}

	public void setDelivered(boolean delivered) {
		this.delivered = delivered;
	}

	public Exercise8_Client getClient() {
		return client;
	}

	public void setClient(Exercise8_Client client) {
		this.client = client;
	}

	public List<Exercise8_OrderRow> getOrderRows() {
		return orderRows;
	}

	public void setOrderRows(List<Exercise8_OrderRow> orderRows) {
		this.orderRows = orderRows;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("id: " + id);
		sb.append("\nprice: " + price);
		sb.append("\ndelivered: " + delivered);
		sb.append("\nclient: " + client);
		sb.append("\norderRows: " + orderRows);
		return sb.toString();
	}
}
