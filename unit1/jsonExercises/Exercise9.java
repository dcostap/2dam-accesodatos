import java.io.*;
import java.util.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.thoughtworks.xstream.XStream;

import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.w3c.dom.*;

public class Exercise9 {
	public static void main(String[] args) throws Exception {
		List<Exercise9_Client> clientList = new ArrayList<>();
		try (FileReader ficheiro = new FileReader("clients.json")) {
			JSONParser parser = new JSONParser();
			JSONObject doc = (JSONObject) parser.parse(ficheiro);

			JSONArray clients = (JSONArray) doc.get("clients");
			for (Object object : clients) {
				JSONObject clientJson = (JSONObject) object;
				Exercise9_Client client = new Exercise9_Client();
				clientList.add(client);
				client.setDni((String) clientJson.get("dni"));
				client.setName((String) clientJson.get("name"));
				client.setSurname((String) clientJson.get("surname"));

				JSONArray addressArray = (JSONArray) clientJson.get("addresses");
				Exercise9_Address[] addresses = new Exercise9_Address[addressArray.size()];
				client.setAddresses(addresses);

				int i = 0;
				for (Object addressObject : addressArray) {
					JSONObject addressJson = (JSONObject) addressObject;
					Exercise9_Address address = new Exercise9_Address();
					address.setStreet((String) addressJson.get("street"));
					address.setNumber(Integer.parseInt((String) addressJson.get("number")));
					address.setPostalCode(Integer.parseInt((String) addressJson.get("postalCode")));
					addresses[i] = address;
					i++;
				}
			}
		}

		// serialize objects
		FileOutputStream outputStream = new FileOutputStream("clients.dat");
		ObjectOutputStream fileStream = new ObjectOutputStream(outputStream);

		for (Exercise9_Client o : clientList) {
			fileStream.writeObject(o);
		}

		fileStream.flush();
		outputStream.flush();

		fileStream.close();
		outputStream.close();

		createXStreamXml(clientList);

		createManualXml(clientList);

		createNoAddressXml();
	}

	private static void createXStreamXml(List<Exercise9_Client> clientList) throws FileNotFoundException {
		XStream xstream = new XStream();
		xstream.setMode(XStream.NO_REFERENCES);
		xstream.alias("client", Exercise9_Client.class);
		xstream.alias("clients", List.class);
		xstream.alias("address", Exercise9_Address.class);
		xstream.toXML(clientList, new FileOutputStream("clientsXStream.xml"));
	}

	private static void createManualXml(List<Exercise9_Client> clientList) throws ParserConfigurationException,
			TransformerConfigurationException, TransformerFactoryConfigurationError, TransformerException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		DOMImplementation implementation = builder.getDOMImplementation();

		Document document = implementation.createDocument(null, "clients", null);
		document.setXmlVersion("1.0");

		for (Exercise9_Client client : clientList) {
			Element clientXml = document.createElement("client");
			clientXml.setAttribute("dni", client.getDni());
			clientXml.setAttribute("name", client.getName());
			clientXml.setAttribute("surname", client.getSurname());
			document.getDocumentElement().appendChild(clientXml);

			for (Exercise9_Address address : client.getAddresses()) {
				if (address == null)
					continue;
				Element addressXml = document.createElement("address");
				clientXml.appendChild(addressXml);
				addressXml.setAttribute("postalCode", String.valueOf(address.getPostalCode()));

				Element street = document.createElement("street");
				street.setTextContent(address.getStreet());
				addressXml.appendChild(street);

				Element number = document.createElement("number");
				number.setTextContent(String.valueOf(address.getNumber()));
				addressXml.appendChild(number);
			}
		}

		Source fonte = new DOMSource(document);
		Result resultado = new StreamResult(new java.io.File("clients.xml"));
		Transformer transformador = TransformerFactory.newInstance().newTransformer();
		transformador.transform(fonte, resultado);
	}

	private static void createNoAddressXml() {
		// read xml and delete addresses
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(new File("clients.xml"));
			document.getDocumentElement().normalize();

			NodeList addresses = document.getElementsByTagName("address");
			for (int i = addresses.getLength() - 1; i >= 0; i--) {
				Element address = (Element) addresses.item(i);
				address.getParentNode().removeChild(address);
			}

			Source fonte = new DOMSource(document);
			Result resultado = new StreamResult(new java.io.File("clientsNoAddress.xml"));
			Transformer transformador = TransformerFactory.newInstance().newTransformer();
			transformador.transform(fonte, resultado);
		} catch (Exception error) {
			error.printStackTrace();
		}
	}
}
