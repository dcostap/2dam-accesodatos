import java.io.Serializable;

public class Exercise8_Product implements Serializable {
	private int price;
	private int id;
	private String productName;
	private String description;
	private String picture;

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	@Override
	public String toString() {
		return "Exercise8_Product [description=" + description + ", id=" + id + ", picture=" + picture + ", price="
				+ price + ", productName=" + productName + "]";
	}
}
