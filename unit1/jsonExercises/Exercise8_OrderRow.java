import java.io.Serializable;

public class Exercise8_OrderRow implements Serializable {
	private int price;
	private int amount;
	private Exercise8_Product product;

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Exercise8_Product getProduct() {
		return product;
	}

	public void setProduct(Exercise8_Product product) {
		this.product = product;
	}

	public String toString() {
		return "Exercise8_OrderRow [amount=" + amount + ", price=" + price + ", product=" + product + "]";
	}
}