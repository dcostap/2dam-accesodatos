import java.io.Serializable;

public class Exercise9_Client implements Serializable {
	private String dni;
	private String name;
	private String surname;
	private Exercise9_Address[] addresses;

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Exercise9_Address[] getAddresses() {
		return addresses;
	}

	public void setAddresses(Exercise9_Address[] addresses) {
		this.addresses = addresses;
	}

}