# XPath

// -> pick anywhere
[] -> condition
[element] -> pick if it has element
[@attribute] -> pick if attribute exists
[@attribute = "something"]

[condition1 and condition2]
//book[(cond1 or cond2) and cond3]
//book[not(condition)] -> inverts condition inside ()

//book[position() <= 2] -> position() = pos. among all the results of the pick command
//book[last()]

/@attribute -> pick value  of attribute
//book/@* -> all attributes of book

* -> everything
//* -> everything everywhere
/*/book -> elements book inside every top level element

# XQuery

for $var in <expresión xpath>
where <condición Xpath> // filtro de for

let $var := valor

order by <expresión>

for $zon in $doc//zona
order by $zon/cod_zona
return $zon

return <xml>{xquery}</xml>
return
    if ($var='something')
    then <xml></xml>
    else () // return nothing

let $string :=
    if (contains($prod/denominacion, "Placa Base")) then
        "placa"
    else
        "something"

something/text() -> get text contents
data(something) -> same

**update <comando>**

comandos:

update **insert**
            <contents>
        into / following / preceding
            <destination>
update insert
    <zona>
    <cod_zona>10</cod_zona>
    <nombre>Madrid-CENTRO</nombre>
    <director>Pedro Martín</director>
    </zona>
preceding doc("/db/coleccionGimnasio/zonas.xml")/zonas/zon[cod_zona=20]

update **replace**
            /zonas/zona[cod_zona=30]/director
        with
            <directora>Pilar Martín</directora>

update **value**
        /zonas/zona[cod_zona=30]/directora
    with 'Pilar'

update **delete** /zonas/zona[cod_zona=10]

update **rename**
        /zonas/zona[cod_zona=30]/directora
    as 'director'

avg()
count()
sum()

distinct-values()
for $ofi in distinct-values($docum/EMPLEADOS/EMP_ROW/OFICIO)

starts-with()
for $element in stuff
where starts-with($element, 'P')

concat("", $var, "") -> returns string
    use it to hardcode returning custom xml
        concat("<custom", $var, ">")

max()
min()
contains($var, 'text')

doc()
    **this:**
    for $dep in doc("/db/something.xml")
    return
        $dep
    **equals:**
    doc("/db/something.xml")