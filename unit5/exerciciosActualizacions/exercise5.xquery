let $doc := doc("/gimnasio/productos.xml")

return<productos>
	<TITULO>DATOS DE LA TABLA PRODUCTOS</TITULO>
	{
        for $prod in $doc/productos/produc
        return
	<produc>
		{$prod/cod_prod}
		{$prod/denominacion}
		{$prod/precio}
		{$prod/stock_actual}
		{$prod/stock_minimo}
		{$prod/cod_zona}
	</produc>
	}
</productos>


5_1:
let $doc := doc("/gimnasio/productos.xml")
return<productos>
	<TITULO>DATOS DE LA TABLA PRODUCTOS</TITULO>
	{
        for $prod in $doc/productos/produc
        return
	<produc>
		{$prod/cod_prod}
		{$prod/denominacion}
		<precio>{$prod/precio/text() * 1.03}</precio>
		{$prod/stock_actual}
		{$prod/stock_minimo}
		{$prod/cod_zona}
	</produc>
	}
</productos>

5_2:
let $doc := doc("/gimnasio/productos.xml")
return<productos>
	<TITULO>DATOS DE LA TABLA PRODUCTOS</TITULO>
	{
        for $prod in $doc/productos/produc
        return
	<produc>
		{$prod/cod_prod}
		{$prod/denominacion}
		{$prod/precio}
		<stock_actual>{$prod/stock_actual/text() + 10}</stock_actual>
		{$prod/stock_minimo}
		{$prod/cod_zona}
	</produc>
	}
</productos>

5_3
let $doc := doc("/gimnasio/productos.xml")
return<productos>
	<TITULO>DATOS DE LA TABLA PRODUCTOS</TITULO>
	{
        for $prod in $doc/productos/produc
        return
	<produc>
		{$prod/cod_prod}
		{$prod/denominacion}
		{$prod/precio}
		{$prod/stock_actual}
		{$prod/stock_minimo}
		{$prod/cod_zona}
	</produc>
	}
</productos>

5_4
let $doc := doc("/gimnasio/productos.xml")
let $zoneDoc := doc("/gimnasio/zonas.xml")

return<productos>
	<TITULO>DATOS DE LA TABLA PRODUCTOS</TITULO>
	{
        for $prod in $doc/productos/produc
        return
	<produc>
		{$prod/cod_prod}
		{$prod/denominacion}
		{$prod/precio}
		{$prod/stock_actual}
		{$prod/stock_minimo}
		<cod_zona>{$zoneDoc/zonas/zona[cod_zona = 10]/nombre/text()}</cod_zona>
	</produc>
	}
</productos>

5_5
let $doc := doc("/gimnasio/productos.xml")

return<productos>
	<TITULO>DATOS DE LOS PRODUCTOS</TITULO>
	{
        for $prod in $doc/productos/produc
        return
	<produc>
		{$prod/cod_prod}
		{$prod/denominacion}
		{$prod/precio}
		{$prod/stock_actual}
		{$prod/stock_minimo}
		{$prod/cod_zona}
	</produc>
	}
</productos>