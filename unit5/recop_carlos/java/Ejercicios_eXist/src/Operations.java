import org.xmldb.api.base.*;
import org.xmldb.api.modules.*;


import java.io.*;

import org.xmldb.api.*;
import javax.xml.transform.OutputKeys;

public class Operations {

	Collection coleccion;
	final static String USUARIO = "admin";
	final static String CLAVE = "abc123.";
	
	
	public Operations(String URI) {
		try {

			String driver = "org.exist.xmldb.DatabaseImpl"; // Driver para eXist
			Class c1 = Class.forName(driver); // Carga el driver
			Database database = (Database) c1.newInstance();
			database.setProperty("create-database", "true");
			DatabaseManager.registerDatabase(database);
			coleccion = DatabaseManager.getCollection(URI, USUARIO, CLAVE);
			coleccion.setProperty(OutputKeys.INDENT, "no");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public String createCollection() {
		String URI ="";
		try {
		        //Crear unha nova coleccion
		        CollectionManagementService mgtService =(CollectionManagementService)coleccion.getService("CollectionManagementService","1.0");
		        mgtService.createCollection("GIMNASIO");
		       
		        // Acceder á colección que acabo de engadir
		        URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/GIMNASIO";
		        coleccion = DatabaseManager.getCollection(URI,USUARIO,CLAVE);
		        
		        
		        String []nombresArchivos= {"actividades_gim.xml","socios_gim.xml","uso_gimnasio.xml"};
		        
		        for(int i=0; i<nombresArchivos.length; i++) {
		        	
		        	File arquivo=new File(nombresArchivos[i]);
		        	
			        if(!arquivo.canRead()) {
			        	System.out.println("Erro ó ler o ficheiro");
			        }
			        else {
			        	Resource novoRecurso = coleccion.createResource(arquivo.getName(), "XMLResource");
			        	novoRecurso.setContent(arquivo);
			        	coleccion.storeResource(novoRecurso);
			        }		        	
		        }
		        
		       // coleccion.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return URI;
	}
	
	
	
	
	
	
	

}
