import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.xml.transform.OutputKeys;

import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.CompiledExpression;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.CollectionManagementService;
import org.xmldb.api.modules.XQueryService;

public class Operations {
	private static XQueryService xqs;
	private static Collection col;
	private static Scanner sc = new Scanner(System.in);
	private static String usu = "admin";
	private static String usuPwd = "abc123.";

	public static void initialize() throws Exception {
		String driver = "org.exist.xmldb.DatabaseImpl";
		Class c1 = Class.forName(driver);

		Database database = (Database) c1.newInstance();
		database.setProperty("create-database", "true");

		DatabaseManager.registerDatabase(database);

		String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db";
		col = DatabaseManager.getCollection(URI, usu, usuPwd);
		col.setProperty(OutputKeys.INDENT, "no");

		xqs = (XQueryService) col.getService("XQueryService", "1.0");
		xqs.setProperty("indent", "yes");
	}

	public static void close() throws Exception {
		col.close();
	}

	public static void crearColeccion() {
		try {
			// Crear unha nova coleccion
			CollectionManagementService mgtService = (CollectionManagementService) col
					.getService("CollectionManagementService", "1.0");
			mgtService.createCollection("GIMNASIO");

			// Acceder á colección que acabo de engadir
			String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/GIMNASIO";
			col = DatabaseManager.getCollection(URI, usu, usuPwd);

			// Engadir un documento á colección
			String[] files = new String[] { "src/actividades_gim.xml", "src/socios_gim.xml", "src/uso_gimnasio.xml" };

			for (String filename : files) {
				addDocument(filename);
			}
		} catch (Exception erro) {
			erro.printStackTrace();
		}
	}

	private static void addDocument(String filename) {
		File f = new File(filename);
		if (!f.canRead()) {
			System.out.println("Erro ó ler o ficheiro");
		} else {
			Resource novoRecurso;
			try {
				novoRecurso = col.createResource(f.getName(), "XMLResource");
				novoRecurso.setContent(f);
				col.storeResource(novoRecurso);
			} catch (XMLDBException e) {
				e.printStackTrace();
			}
		}
	}

	public static void crearUsoGimnasio() {
		try {
			XQueryService xqs = (XQueryService) col.getService("XQueryService", "1.0");
			xqs.setProperty("indent", "yes");
			CompiledExpression compiled = xqs.compile("" + "xquery version \"3.1\";\n" + "\n"
					+ "let $doc_uso_gimn := doc(\"/db/GIMNASIO/uso_gimnasio.xml\")/USO_GIMNASIO\n"
					+ "let $doc_socios := doc(\"/db/GIMNASIO/socios_gim.xml\")/SOCIOS_GIM\n"
					+ "let $doc_activ := doc(\"/db/GIMNASIO/actividades_gim.xml\")/ACTIVIDADES_GIM\n" + "\n"
					+ "return\n" + "    <USO_GIMNASIO>\n"
					+ "    {for $uso_gimn in $doc_uso_gimn/fila_uso return <fila_uso>\n"
					+ "          <SOCIO>{$doc_socios/fila_socios[COD=$uso_gimn/CODSOCIO]/NOMBRE}</SOCIO>\n"
					+ "          <ACTIV>{$doc_activ/fila_actividades[@cod=$uso_gimn/CODACTIV]/NOMBRE/text()}</ACTIV>\n"
					+ "          {$uso_gimn/FECHA}\n" + "          {$uso_gimn/HORAINICIO}\n"
					+ "          {$uso_gimn/HORAFINAL}\n" + "        </fila_uso>\n" + "    }\n"
					+ "    </USO_GIMNASIO>");
			ResourceSet result = xqs.execute(compiled);

			String filename = "src/uso_gimnasio_modificado.xml";
			PrintWriter ficheiro = new PrintWriter(new FileWriter(filename));

			ResourceIterator i = result.getIterator();
			while (i.hasMoreResources()) {
				Resource res = i.nextResource();
				ficheiro.println(res.getContent());
			}
			ficheiro.close();

			addDocument(filename);
		} catch (Exception erro) {
			erro.printStackTrace();
		}
	}

	public static void actividadesGimnasioSocios() {
		try {
			XQueryService xqs = (XQueryService) col.getService("XQueryService", "1.0");
			xqs.setProperty("indent", "yes");
			CompiledExpression compiled = xqs.compile("" + "xquery version \"3.1\";\n" + "\n"
					+ "let $doc_uso_gimn := doc(\"/db/GIMNASIO/uso_gimnasio.xml\")/USO_GIMNASIO\n"
					+ "let $doc_socios := doc(\"/db/GIMNASIO/socios_gim.xml\")/SOCIOS_GIM\n"
					+ "let $doc_activ := doc(\"/db/GIMNASIO/actividades_gim.xml\")/ACTIVIDADES_GIM\n" + "\n"
					+ "return\n" + "    <ACTIVIDADES_GIM>\n"
					+ "    <numero_total_actividades>{count($doc_activ/fila_actividades)}</numero_total_actividades>\n"
					+ "    {for $activ in $doc_activ/fila_actividades return \n"
					+ "        <fila_actividades cod=\"{$activ/@cod}\" tipo=\"{$activ/@tipo}\">\n"
					+ "          {$activ/NOMBRE}\n"
					+ "          <numero_socios>{count($doc_socios/fila_socios[COD=$activ/@cod])}</numero_socios>\n"
					+ "        </fila_actividades>\n" + "    }\n" + "    </ACTIVIDADES_GIM>    ");
			ResourceSet result = xqs.execute(compiled);

			ResourceIterator i = result.getIterator();
			while (i.hasMoreResources()) {
				Resource res = i.nextResource();
				System.out.println(res.getContent());
			}
		} catch (Exception erro) {
			erro.printStackTrace();
		}
	}

	public static boolean departamentExists(int deptNum) throws Exception {
		CompiledExpression compiled = xqs
				.compile("" + "for $dept in doc(\"/db/gimnasio/departamentos.xml\")/departamentos/filaDepar[DEPT_NO = "
						+ deptNum + "] " + "return $dept");

		ResourceSet result = xqs.execute(compiled);

		ResourceIterator ite = result.getIterator();

		return ite.hasMoreResources();
	}

	public static String mostraDepartamentos() {
		String xquery = "";
		try {
			CompiledExpression exp = xqs
					.compile("" + "for $dept in doc(\"/db/gimnasio/departamentos.xml\") " + "return $dept");

			ResourceSet result = xqs.execute(exp);
			ResourceIterator i = result.getIterator();
			while (i.hasMoreResources()) {
				Resource res = i.nextResource();
				xquery += res.getContent().toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xquery;
	}

	public static boolean modificarDept() throws Exception {
		System.out.println("Inserta número de departamento:");
		int deptNo = Integer.parseInt(sc.nextLine());
		System.out.println("Inserta nombre:");
		String dNombre = sc.nextLine();
		System.out.println("Inserta localidad:");
		String localidade = sc.nextLine();

		if (departamentExists(deptNo)) {
			Departamento dept = new Departamento(deptNo, dNombre, localidade);
			String orde = "update replace\n" + "/departamentos/filaDepar[DEPT_NO=" + dept.getDeptNo() + "]\n"
					+ "with <filaDepar>\n" + "        <DEPT_NO>" + dept.getDeptNo() + "</DEPT_NO>\n"
					+ "        <DNOMBRE>" + dept.getdNombre() + "</DNOMBRE>\n" + "        <LOC>" + dept.getLocalidade()
					+ "</LOC>\n" + "</filaDepar>";
			CompiledExpression compiled = xqs.compile(orde);
			xqs.execute(compiled);
			return true;
		} else {

			return false;
		}
	}

	public static boolean insertDep() {
		try {
			System.out.println("Inserta número de departamento:");
			int deptNo = Integer.parseInt(sc.nextLine());
			System.out.println("Inserta nombre:");
			String dNombre = sc.nextLine();
			System.out.println("Inserta localidad:");
			String localidade = sc.nextLine();

			if (!departamentExists(deptNo)) {
				Departamento dept = new Departamento(deptNo, dNombre, localidade);
				String orde = "update insert\n" + "<filaDepar>\n" + "<DEPT_NO>" + dept.getDeptNo() + "</DEPT_NO>\n"
						+ "<DNOMBRE>" + dept.getdNombre() + "</DNOMBRE>\n" + "<LOC>" + dept.getLocalidade() + "</LOC>\n"
						+ "</filaDepar>\n" + "into doc(\"/db/gimnasio/departamentos.xml\")/departamentos";
				CompiledExpression compiled = xqs.compile(orde);
				xqs.execute(compiled);
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean borrarDep() throws Exception {
		System.out.println("Inserta número de departamento:");
		int deptNo = Integer.parseInt(sc.nextLine());

		if (departamentExists(deptNo)) {
			String orde = "update delete\n" + "/departamentos/filaDepar[DEPT_NO=" + deptNo + "]";
			CompiledExpression exp = xqs.compile(orde);
			xqs.execute(exp);
			return true;
		} else {
			return false;
		}
	}

}
