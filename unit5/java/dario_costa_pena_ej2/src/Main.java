public class Main {

	public static void main(String[] args) {
		try {
			Operations.initialize();

			Operations.crearColeccion();
			System.out.println("Colección GIMNASIO creada dentro de db/");

			Operations.crearUsoGimnasio();

			Operations.actividadesGimnasioSocios();

			System.out.println("Final del programa.");
			Operations.close();
		} catch (Exception e) {
			System.out.println();
			System.out.println("Hubo un error durante la ejecución del programa:");
			System.out.println(e.getMessage());
		}
	}
}
