import org.xmldb.api.base.*;
import org.xmldb.api.modules.*;
import org.xmldb.api.*;
import javax.xml.transform.OutputKeys;
import java.io.File;
import java.util.Scanner;

public class Main {
	private static Scanner sc = new Scanner(System.in);
	private static String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/gimnasio";

	public static void main(String[] args) {
		mostraDepartamentos();
		insertaDep();
	}

	public static void mostraDepartamentos() {
		consulta("for $em in doc(\"/db/gimnasio/departamentos.xml\")/departamentos return $em");
	}

	public static void insertaDep() {
		System.out.println("Introduce nº de departamento:");
		int number = sc.nextInt();
		System.out.println("Introduce nombre de departamento:");
		String name = sc.nextLine();
		System.out.println("Introduce localidad de departamento:");
		String localidad = sc.nextLine();

		consulta("update insert <DEP_ROW>"
		+"<DEPT_NO>99</DEPT_NO>"
		+"<DNOMBRE>CONTABILIDAD</DNOMBRE>"
		+"<LOC>SEVILLA</LOC>"
		  +"</DEP_ROW>"
		  +"into doc(\"/db/gimnasio/departamentos.xml\")");

	}

	public static void insertDep() {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.println("Inserta un DEPT_NO");
            String deptNo = sc.nextLine();
            System.out.println("Inserta un DNOMBRE");
            String dNombre = sc.nextLine();
            System.out.println("Inserta unha Localidade");
            String localidade = sc.nextLine();

            if (!existDepartamento(Integer.parseInt(deptNo))) {
                String orde = "update insert\n"
                        + "<filaDepar>\n"
                        + "<DEPT_NO>" + deptNo + "</DEPT_NO>\n"
                        + "<DNOMBRE>" + dNombre + "</DNOMBRE>\n"
                        + "<LOC>" + localidade + "</LOC>\n"
                        + "</filaDepar>\n"
                        + "into doc(\"db/gimnasio/departamentos.xml\")/departamentos";
                CompiledExpression compiled = xqs.compile(orde);
                xqs.execute(compiled);
            } else {
                System.out.println("O departamento xa existe !");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	public static void borraDep() {

	}

	public static void modificaDep() {

	}

	public static void consulta(String xquery) {
		try {
			String driver = "org.exist.xmldb.DatabaseImpl"; // Driver para eXist
			Class c1 = Class.forName(driver); // Carga o driver

			Database database = (Database) c1.newInstance();
			database.setProperty("create-database", "true");
			DatabaseManager.registerDatabase(database);

			Collection col = DatabaseManager.getCollection(URI);
			col.setProperty(OutputKeys.INDENT, "no");

			XQueryService xqs = (XQueryService) col.getService("XQueryService", "1.0");
			xqs.setProperty("indent", "yes");

			CompiledExpression compiled = xqs.compile(xquery);

			ResourceSet result = xqs.execute(compiled);
			ResourceIterator i = result.getIterator();
			while (i.hasMoreResources()) {
				Resource res = i.nextResource();
				System.out.println(res.getContent());
			}

			col.close();
		} catch (Exception erro) {
			erro.printStackTrace();
		}
	}

	public static void creaColeccion() {
		try {
			String driver = "org.exist.xmldb.DatabaseImpl"; // Driver para eXist
			Class c1 = Class.forName(driver); // Carga o driver

			Database database = (Database) c1.newInstance();
			database.setProperty("create-database", "true");
			DatabaseManager.registerDatabase(database);

			String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db";
			String usu = "admin"; // usuario
			String usuPwd = "abc123."; // clave

			Collection col = DatabaseManager.getCollection(URI, usu, usuPwd);
			col.setProperty(OutputKeys.INDENT, "no");

			if (col == null)
				System.out.println("A coleccion non existe.");

			// Crear unha nova coleccion
			CollectionManagementService mgtService = (CollectionManagementService) col
					.getService("CollectionManagementService", "1.0");
			// mgtService.createCollection("novaColeccion2");

			// Borrar unha coleccion
			// mgtService.removeCollection("novaColeccion2");

			// Acceder á colección que acabo de engadir
			URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/novaColeccion2";
			// col = DatabaseManager.getCollection(URI,usu,usuPwd);

			// Engadir un documento á colección
			File arquivo = new File("departamentos.xml");

			if (!arquivo.canRead()) {
				System.out.println("Erro ó ler o ficheiro");
			} else {
				Resource novoRecurso = col.createResource(arquivo.getName(), "XMLResource");
				novoRecurso.setContent(arquivo);
				col.storeResource(novoRecurso);
			}

			// Borrar un documento dunha coleccion
			Resource recursoParaBorrar = col.getResource("departamentos.xml");
			// col.removeResource(recursoParaBorrar);

			col.close();
		} catch (Exception erro) {
			System.out.println("Erro en creaColeccion " + erro.getMessage());
		}

	}
}// class
