
public class Departamento {
	int deptNo;
	String dNombre;
	String localidade;

	public Departamento(int deptNo, String dNombre, String localidade) {
		super();
		this.deptNo = deptNo;
		this.dNombre = dNombre;
		this.localidade = localidade;
	}

	public int getDeptNo() {
		return deptNo;
	}

	public void setDeptNo(int deptNo) {
		this.deptNo = deptNo;
	}

	public String getLocalidade() {
		return localidade;
	}

	public void setLocalidade(String localidade) {
		this.localidade = localidade;
	}

	public String getdNombre() {
		return dNombre;
	}

	public void setdNombre(String dNombre) {
		this.dNombre = dNombre;
	}

	@Override
	public String toString() {
		return "Departamento [deptNo=" + deptNo + ", dNombre=" + dNombre + ", localidade=" + localidade + "]";
	}
}
