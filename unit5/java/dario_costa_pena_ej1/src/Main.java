public class Main {

	public static void main(String[] args) {
		try {
			Operations.initialize();

			System.out.println("Todos los departamentos: ");
			System.out.println(Operations.mostraDepartamentos());
			System.out.println();
			System.out.println("Añadiendo un departamento...");
			if (Operations.insertDep()) {
				System.out.println("Departamento añadido");
			} else {
				System.out.println("Han habido problemas añadiendo el departamento");
			}
			System.out.println();
			System.out.println("Borrando un departamento...");
			if (Operations.borrarDep()) {
				System.out.println("Departamento borrado");
			} else {
				System.out.println("El departamento no existía");
			}
			System.out.println();
			System.out.println("Modificando un departamento...");
			if (Operations.modificarDept()) {
				System.out.println("Departamento modificado");
			} else {
				System.out.println("Han habido problemas modificando el departamento");
			}

			System.out.println("Final del programa.");
			Operations.close();
		} catch (Exception e) {
			System.out.println();
			System.out.println("Hubo un error durante la ejecución del programa:");
			System.out.println(e.getMessage());
		}
	}
}
