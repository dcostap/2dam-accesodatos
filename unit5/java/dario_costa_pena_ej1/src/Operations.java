import java.util.Scanner;

import javax.xml.transform.OutputKeys;

import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.CompiledExpression;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.Resource;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.modules.XQueryService;

public class Operations {
	private static XQueryService xqs;
	private static Collection col;
	private static Scanner sc = new Scanner(System.in);

	public static void initialize() throws Exception {
		String driver = "org.exist.xmldb.DatabaseImpl";
		Class c1 = Class.forName(driver);

		Database database = (Database) c1.newInstance();
		database.setProperty("create-database", "true");
		String usu = "admin";
		String usuPwd = "abc123.";
		DatabaseManager.registerDatabase(database);

		String URI = "xmldb:exist://localhost:8080/exist/xmlrpc/db/gimnasio";
		col = DatabaseManager.getCollection(URI, usu, usuPwd);
		col.setProperty(OutputKeys.INDENT, "no");

		xqs = (XQueryService) col.getService("XQueryService", "1.0");
		xqs.setProperty("indent", "yes");
	}

	public static void close() throws Exception {
		col.close();
	}

	public static boolean departamentExists(int deptNum) throws Exception {
		CompiledExpression compiled = xqs
				.compile("" + "for $dept in doc(\"/db/gimnasio/departamentos.xml\")/departamentos/filaDepar[DEPT_NO = "
						+ deptNum + "] " + "return $dept");

		ResourceSet result = xqs.execute(compiled);

		ResourceIterator ite = result.getIterator();

		return ite.hasMoreResources();
	}

	public static String mostraDepartamentos() {
		String xquery = "";
		try {
			CompiledExpression exp = xqs
					.compile("" + "for $dept in doc(\"/db/gimnasio/departamentos.xml\") " + "return $dept");

			ResourceSet result = xqs.execute(exp);
			ResourceIterator i = result.getIterator();
			while (i.hasMoreResources()) {
				Resource res = i.nextResource();
				xquery += res.getContent().toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xquery;
	}

	public static boolean modificarDept() throws Exception {
		System.out.println("Inserta número de departamento:");
		int deptNo = Integer.parseInt(sc.nextLine());
		System.out.println("Inserta nombre:");
		String dNombre = sc.nextLine();
		System.out.println("Inserta localidad:");
		String localidade = sc.nextLine();

		if (departamentExists(deptNo)) {
			Departamento dept = new Departamento(deptNo, dNombre, localidade);
			String orde = "update replace\n" + "/departamentos/filaDepar[DEPT_NO=" + dept.getDeptNo() + "]\n"
					+ "with <filaDepar>\n" + "        <DEPT_NO>" + dept.getDeptNo() + "</DEPT_NO>\n"
					+ "        <DNOMBRE>" + dept.getdNombre() + "</DNOMBRE>\n" + "        <LOC>" + dept.getLocalidade()
					+ "</LOC>\n" + "</filaDepar>";
			CompiledExpression compiled = xqs.compile(orde);
			xqs.execute(compiled);
			return true;
		} else {

			return false;
		}
	}

	public static boolean insertDep() {
		try {
			System.out.println("Inserta número de departamento:");
			int deptNo = Integer.parseInt(sc.nextLine());
			System.out.println("Inserta nombre:");
			String dNombre = sc.nextLine();
			System.out.println("Inserta localidad:");
			String localidade = sc.nextLine();

			if (!departamentExists(deptNo)) {
				Departamento dept = new Departamento(deptNo, dNombre, localidade);
				String orde = "update insert\n" + "<filaDepar>\n" + "<DEPT_NO>" + dept.getDeptNo() + "</DEPT_NO>\n"
						+ "<DNOMBRE>" + dept.getdNombre() + "</DNOMBRE>\n" + "<LOC>" + dept.getLocalidade() + "</LOC>\n"
						+ "</filaDepar>\n" + "into doc(\"/db/gimnasio/departamentos.xml\")/departamentos";
				CompiledExpression compiled = xqs.compile(orde);
				xqs.execute(compiled);
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean borrarDep() throws Exception {
		System.out.println("Inserta número de departamento:");
		int deptNo = Integer.parseInt(sc.nextLine());

		if (departamentExists(deptNo)) {
			String orde = "update delete\n" + "/departamentos/filaDepar[DEPT_NO=" + deptNo + "]";
			CompiledExpression exp = xqs.compile(orde);
			xqs.execute(exp);
			return true;
		} else {
			return false;
		}
	}

}
