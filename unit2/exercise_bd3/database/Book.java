package database;

public class Book {
    public int idBook;
    public String code;
    public String title;
    public String authors;
    public int year;

    @Override
    public String toString() {
        return "Book{" +
                "idBook=" + idBook +
                ", code='" + code + '\'' +
                ", title='" + title + '\'' +
                ", authors='" + authors + '\'' +
                ", year=" + year +
                '}';
    }
}
