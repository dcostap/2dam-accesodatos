package database;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Loan {
    public static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public int idLoan;
    public Book book;
    public Client client;
    public Date date;
    public boolean borrowed;

    @Override
    public String toString() {
        return "Loan{" +
                "idLoan=" + idLoan +
                ", book=" + book +
                ", client=" + client +
                ", date=" + date +
                ", borrowed=" + borrowed +
                '}';
    }
}
