package database;

public class Order {
	public int idPedido;
	public Product product;
	public Client client;
	public double amount;

	@Override
	public String toString() {
		return "Order{" +
				"idPedido=" + idPedido +
				", product=" + product +
				", client=" + client +
				", amount=" + amount +
				'}';
	}
}
