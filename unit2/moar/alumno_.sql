use probas;
create table alumno (id int primary key AUTO_INCREMENT, dni varchar(9), nome varchar(100), apelidos varchar(200), idade int);
insert into alumno (dni, nome, apelidos, idade) values ('11111111A', 'Pepe', 'Perez', 20);
insert into alumno (dni, nome, apelidos, idade) values ('22222222B', 'Maria', 'Ferradas', 20);
insert into alumno (dni, nome, apelidos, idade) values ('33333333C', 'Laura', 'Agulla', 20);
select * from alumno;

