import database.Client;
import database.Operations;
import database.Order;
import database.Product;

public class Program {
	public static void main(String[] args) {
		Operations op = new Operations();
		op.openConnection();

		System.out.println("Lista de productos: ");
		System.out.println(op.ordersList());

		System.out.println("Existe el producto con id 1?");
		System.out.println(op.existsProduct(1));

		System.out.println("Contenido de producto con id 1");
		System.out.println(op.getProduct(1));

		System.out.println("Existe el producto con id 10000?");
		System.out.println(op.existsProduct(10000));

		System.out.println("Existe el cliente con id 1?");
		System.out.println(op.existsClient(1));

		System.out.println("Muestra el cliente con id 1");
		System.out.println(op.getClient(1));

		Order order = new Order();
		order.idPedido = 10;

		Product product = new Product();
		product.idProduct = 1;
		order.product = product;

		Client client = new Client();
		client.idClient = 1;
		order.client = client;

		op.addOrder(order);

		System.out.println("Added order:");
		op.getOrder(order.idPedido);

		Order order2 = new Order();
		order2.idPedido = 10;

		Product product2 = new Product();
		product2.idProduct = 20;
		product2.description = "description2";
		product2.name = "nameOfProduct2";
		product2.picture = "picture2.jpg";
		product2.price = 2.0;
		order2.product = product2;

		Client client2 = new Client();
		client2.idClient = 10;
		client2.dni = "111111J";
		client2.name = "nameOfClient2";
		order2.client = client2;

		op.modifyOrder(order2);

		System.out.println(op.ordersList());

		op.closeConnection();
	}
}
