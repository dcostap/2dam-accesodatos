package operacions;

import java.sql.*;
import java.util.*;

public class OperacionsBd{
  private Connection conexion=null;
  private String erro="";
  public String getErro(){
    return erro;
  }
  public void abrirConexion(){
    try{
      //Carga do controlador JDBC de MySQL
      Class.forName("com.mysql.jdbc.Driver");
      //Crea unha conexion a base de datos
      String url="jdbc:mysql://localhost/exemplo";
      conexion = DriverManager.getConnection(url,"usuario","abc123.");
    }
    catch(SQLException error){
      erro=erro+"\nErro SQL en abrirConexion: "+error.getMessage();
    }
    catch(Exception error){
      erro=erro+"\nErro en abrirConexion: "+error.getMessage();
    }
  }//abrirConexion
  public void cerrarConexion(){
    if (conexion != null){
        try{
          conexion.close();
        }
        catch(SQLException error){
          erro=erro+"\nErro SQL en cerrarConexion: "+error.getMessage();
        }
        catch(Exception error){
          erro=erro+"\nErro en cerrarConexion: "+error.getMessage();
        }
      }//if
  }//cerrarConexion
//Metodo que recibe un alumno e o engade na base de datos
  //Devolve 1 en caso de que a operacion se realizase con exito
  public int engadeAlumno(Alumno alumno){
    PreparedStatement sentencia = null;
    int resultado = 0;
    try{
      sentencia = conexion.prepareStatement("insert into alumno(DNI,nome, apelidos, idade) values (?,?,?,?)");
      sentencia.setString(1, alumno.getDNI());
      sentencia.setString(2, alumno.getNome());
      sentencia.setString(3, alumno.getApelidos());
      sentencia.setInt(4, alumno.getIdade());
      resultado = sentencia.executeUpdate();
    }
    catch(SQLException ex){
      erro += "\nErro SQL en engadeAlumno: "+ex.getMessage();
    }
    catch(Exception ex){
      erro = erro+"\nErro en engadeAlumno: "+ex.getMessage();
    }
    finally{
      if (sentencia != null){
        try{
          sentencia.close();
        }
        catch(Exception err){
          erro = erro+"\nErro en engadeAlumno o cerrar PreparedStatement: "+err.getMessage();
        }
      }//if
    }
    return resultado;
  }//engadeAlumno
  //Metodo que recibe un dni e busca o alumno correspondente na BD
  //Devolve o obxecto alumno atopado, ou null en caso de que non estea na BD
  public Alumno consultaAlumno(String dni){
    PreparedStatement sentencia = null;
    Alumno unAlumno = null;
    ResultSet datos = null;
    try{
      sentencia = conexion.prepareStatement("select * from alumno where DNI=?");
      sentencia.setString(1, dni);
      datos = sentencia.executeQuery();
      if (datos.next())//Se ten datos
        unAlumno = new Alumno(datos.getString("DNI"),datos.getString("nome"),datos.getString("apelidos"),datos.getInt("idade"));
    }
    catch(SQLException ex){
      erro += "\nErro SQL en consultaAlumno: "+ex.getMessage();
    }
    catch(Exception ex){
      erro = erro+"\nErro en consultaAlumno: "+ex.getMessage();
    }
    finally{
      if (sentencia != null){
        try{
          sentencia.close();
        }
        catch(Exception err){
          erro = erro+"\nErro en consultaAlumno o cerrar PreparedStatement: "+err.getMessage();
        }
      }//if
      if (datos != null){
        try{
          datos.close();
        }
        catch(Exception err){
          erro = erro+"\nErro en consultaAlumno o cerrar ResultSet: "+err.getMessage();
        }
      }//if
    }
    return unAlumno;
  }//consultaAlumno
//Metodo que devolve un ArrayList co contido da taboa alumno 
  public ArrayList<Alumno> listadoAlumnos() {
    Statement sentencia = null;
    ResultSet datos = null;
    Alumno alumno = null;
    ArrayList<Alumno> listaAlumnos = new ArrayList<Alumno>();
    try{
      sentencia = conexion.createStatement();
      datos = sentencia.executeQuery("select * from alumno");
      while (datos.next()){
        alumno = new Alumno(datos.getString("DNI"),datos.getString("nome"),datos.getString("apelidos"),datos.getInt("idade"));
        listaAlumnos.add(alumno);
      }//while
    }//try
    catch(SQLException ex){
      erro += "\nErro SQL en listadoAlumno: "+ex.getMessage();
    }
    catch(Exception ex){
      erro = erro+"\nErro en listadoAlumnos: "+ex.getMessage();
    }
    finally{
      if (datos != null){
        try{
          datos.close();
        }
        catch(Exception err){
          erro = erro+"\nErro en listadoAlumnos o cerrar ResultSet: "+err.getMessage();
        }
      }//if
      if (sentencia != null){
        try{
          sentencia.close();
        }
        catch(Exception err){
          erro = erro+"\nErro en listadoAlumno o cerrar PreparedStatement: "+err.getMessage();
        }
      }//if
    }
    return listaAlumnos;
  }//listadoAlumnos
  //Metodo que recibe un dni e borra o alumno correspondente na BD
  //Devolve un 1 si se realizou o borrado e un 0 en caso contrario
  public int borraAlumno(String dni){
    PreparedStatement sentencia = null;
    Alumno unAlumno = null;
    int resultado = 0;
    try{
      sentencia = conexion.prepareStatement("delete from alumno where DNI=?");
      sentencia.setString(1, dni);
      resultado = sentencia.executeUpdate();
    }
    catch(SQLException ex){
      erro += "\nErro SQL en consultaAlumno: "+ex.getMessage();
    }
    catch(Exception ex){
      erro = erro+"\nErro en consultaAlumno: "+ex.getMessage();
    }
    finally{
      if (sentencia != null){
        try{
          sentencia.close();
        }
        catch(Exception err){
          erro = erro+"\nErro en borraAlumno o cerrar PreparedStatement: "+err.getMessage();
        }
      }//if
    }
    return resultado;
  }//borraAlumno
  //Metodo que recibe un alumno, usa o dni para buscalo e os demais campos para modificalo
  //Devolve un 1 si se realizou a modificacion e un 0 en caso contrario
  public int modificaAlumno(Alumno alumno){
    PreparedStatement sentencia = null;
    int resultado = 0;
    try{
      sentencia = conexion.prepareStatement("update alumno set nome=?, apelidos=?, idade=? where DNI=?");
      sentencia.setString(1, alumno.getNome());
      sentencia.setString(2, alumno.getApelidos());
      sentencia.setInt(3, alumno.getIdade());
      sentencia.setString(4, alumno.getDNI());
      resultado = sentencia.executeUpdate();
    }
    catch(SQLException ex){
      erro += "\nErro SQL en modificaAlumno: "+ex.getMessage();
    }
    catch(Exception ex){
      erro = erro+"\nErro en modificaAlumno: "+ex.getMessage();
    }
    finally{
      if (sentencia != null){
        try{
          sentencia.close();
        }
        catch(Exception err){
          erro = erro+"\nErro en modificaAlumno o cerrar PreparedStatement: "+err.getMessage();
        }
      }//if
    }
    return resultado;
  }//modificaAlumno
}//class
