package operacions;
public class Alumno{
  private String DNI;
  private String nome;
  private String apelidos;
  private int idade;
  public Alumno(String DNI,String nome,String apelidos,int idade){
    this.DNI=DNI;
    this.nome=nome;
    this.apelidos=apelidos;
    this.idade=idade;
  }
  public Alumno(){}
  public String getDNI(){
    return DNI;
  }
  public String getNome(){
    return nome;
  }
  public String getApelidos(){
    return apelidos;
  }
  public int getIdade(){
    return idade;
  }
  public String toString(){
    String cadeaAlumno="DNI: "+DNI+", Nome: "+nome+", Apelidos: "+apelidos+", Idade: "+idade;
    return cadeaAlumno;
  }
  
}//clase
