<%@page import=operacions.*"%>
<html>
  <head>
  
  </head>
  <body>
    <%
    OperacionsBd baseDatos = new OperacionsBd();

    //ABRIR CONEXION
    baseDatos.abrirConexion();

    //REALIZAR OPERACIONS
    //Engadir un alumno
    out.println("ENGADIR UN ALUMNO");
    //Pedir introducion de datos por consola
    Alumno unAlumno = new Alumno("56785678A","Xurxo","Alvarez",10);
    int resultado = baseDatos.engadeAlumno(unAlumno);
    if (resultado==0) out.println("Non se engadiu ningun alumno.");
    //CERRAR CONEXION
    baseDatos.cerrarConexion();

    //MOSTRAR OS ERROS QUE SE PRODUCIRON
    if (baseDatos.getErro()!="")
      out.println("\nERROS: "+baseDatos.getErro());

    %>
  </body>
</html>
