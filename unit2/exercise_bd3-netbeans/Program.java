import java.sql.SQLException;
import java.util.Arrays;

import database.Client;
import database.Operations;
import database.Order;
import database.Product;

public class Program {
	public static void main(String[] args) {
		Operations op = new Operations();
		try {
			op.openConnection();

			System.out.println("Does client with id 1 exist?");
			System.out.println(op.existsClient(1));

			System.out.println("Does client with id 44 exist?");
			System.out.println(op.existsClient(44));

			System.out.println("Does book with id 1 exist?");
			System.out.println(op.existsBook(1));

			System.out.println("Contents of book with id 1:");
			System.out.println(op.getBook(1));

			System.out.println("Does book with id 77 exist?");
			System.out.println(op.existsBook(77));

			System.out.println("Is book with code asrwqeg borrowed?");
			System.out.println(op.isBorrowed("asrwqeg"));

			System.out.println("Is book with code asdfg borrowed?");
			System.out.println(op.isBorrowed("asdfg"));

			System.out.println("Loaning book with code asdfg...");
			op.addLoan("asdfg", 1);

			System.out.println("Is book with code asdfg borrowed?");
			System.out.println(op.isBorrowed("asdfg"));

			System.out.println("All borrowed books:");
			op.borrowedBooksList().forEach(System.out::println);

			System.out.println("Returning previous book...");
			op.addReturn("asdfg");

			System.out.println("All borrowed books:");
			op.borrowedBooksList().forEach(System.out::println);

			System.out.println("All available books:");
			op.availableBooksList().forEach(System.out::println);

			op.closeConnection();
		} catch (ClassNotFoundException e) {
			System.out.println("There was a problem finding a class!");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("There was a SQL error!");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("There was an unidentified error!");
			e.printStackTrace();
		}
	}
}
