package database;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import database.Client;
import database.Order;
import database.Product;

public class Operations {
    private Connection conn;

    public void openConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://192.168.56.101/library";
        conn = DriverManager.getConnection(url, "libraryManager", "abc123.");
    }

    public void closeConnection() throws SQLException {
        conn.close();
    }

    public boolean existsClient(int id) throws SQLException {
        return getClient(id) != null;
    }

    public Client getClient(int id) throws SQLException {
        Client client = null;
        PreparedStatement statement = conn.prepareStatement("select * from client where idClient like ?");

        statement.setString(1, Integer.toString(id));
        ResultSet data = statement.executeQuery();
        while (data.next()) {
            client = new Client();
            client.email = data.getString("email");
            client.name = data.getString("name");
            client.dni = data.getString("DNI");
            client.idClient = data.getInt("idClient");
        }

        return client;
    }

    public boolean existsBook(int id) throws SQLException {
        return getBook(id) != null;
    }

    public Book getBook(int id) throws SQLException {
        Book book = null;
        PreparedStatement statement = conn.prepareStatement("select * from book where idBook like ?");

        statement.setString(1, Integer.toString(id));
        ResultSet data = statement.executeQuery();
        while (data.next()) {
            book = new Book();
            book.authors = data.getString("authors");
            book.title = data.getString("title");
            book.idBook = data.getInt("idBook");
            book.code = data.getString("code");
            book.year = data.getInt("year");
        }

        return book;
    }

    public Book getBook(String code) throws SQLException {
        Book book = null;
        PreparedStatement statement = conn.prepareStatement("select * from book where code like ?");

        statement.setString(1, code);
        ResultSet data = statement.executeQuery();

        while (data.next()) {
            book = new Book();
            book.authors = data.getString("authors");
            book.title = data.getString("title");
            book.idBook = data.getInt("idBook");
            book.code = data.getString("code");
            book.year = data.getInt("year");
        }

        return book;
    }

    public Loan getLoan(int idLoan) throws SQLException {
        Loan loan = null;
        PreparedStatement statement = conn.prepareStatement("select * from loan where idLoan like ?");

        statement.setInt(1, idLoan);
        ResultSet data = statement.executeQuery();

        while (data.next()) {
            loan = new Loan();
            loan.book = getBook(data.getInt("idBook"));
            loan.client = getClient(data.getInt("idClient"));
            loan.borrowed = data.getBoolean("borrowed");
            loan.date = data.getDate("date");
            loan.idLoan = idLoan;
        }

        return loan;
    }

    public ArrayList<Loan> getLoanList() throws SQLException {
        ArrayList<Loan> loans = new ArrayList<>();
        PreparedStatement statement = conn.prepareStatement("select * from loan");
        ResultSet data = statement.executeQuery();
        while (data.next()) {
            Loan loan = getLoan(data.getInt("idLoan"));
            loans.add(loan);
        }

        return loans;
    }

    public boolean isBorrowed(String bookCode) throws SQLException {
        Book book = getBook(bookCode);
        return getLoanList().stream().anyMatch(loan -> loan.book.idBook == book.idBook && loan.borrowed);
    }

    public int addLoan(String bookCode, int clientId) throws Exception {
        Book book = getBook(bookCode);
        Client client = getClient(clientId);

        if (client == null) {
            throw new Exception("Client with id " + clientId + " does not exist!");
        } else if (book == null) {
            throw new Exception("Book with code " + bookCode + " does not exist!");
        } else if (isBorrowed(bookCode)) {
            throw new Exception("Book with code " + bookCode + " is being borrowed, so it can't be loaned!");
        }

        Loan loan = new Loan();
        loan.book = book;
        loan.client = client;
        loan.borrowed = true;
        loan.idLoan = 0;
        loan.date = new Date(System.currentTimeMillis());
        while (getLoan(loan.idLoan) != null) {
            loan.idLoan = new Random().nextInt(9999);
        }

        addLoan(loan);

        return 0;
    }

    public void addLoan(Loan loan) throws SQLException {
        PreparedStatement statement = conn
                .prepareStatement("insert into loan (idLoan, idBook, idClient, date, borrowed) values (?,?,?,?,?);");

        if (!existsClient(loan.client.idClient))
            throw new RuntimeException("Client with id " + loan.client.idClient + " doesn't exist in the database.");
        if (!existsBook(loan.book.idBook))
            throw new RuntimeException("Book with id " + loan.book.idBook + " doesn't exist in the database.");

        statement.setString(1, String.valueOf(loan.idLoan));
        statement.setString(2, String.valueOf(loan.book.idBook));
        statement.setString(3, String.valueOf(loan.client.idClient));
        statement.setDate(4, loan.date);
        statement.setInt(5, loan.borrowed ? 1 : 0);
        statement.executeUpdate();
    }

    public void addReturn(String bookCode) throws SQLException {
        Book book = getBook(bookCode);

        PreparedStatement statement = conn.prepareStatement("update loan set borrowed = 0 where idBook like ?");
        statement.setInt(1, book.idBook);
        statement.executeUpdate();
    }

    public List<Loan> borrowedBooksList() throws SQLException {
        return getLoanList().stream()
            .filter(loan -> loan.borrowed)
            .collect(Collectors.toList());
    }

    public ArrayList<Book> availableBooksList() throws SQLException {
        ArrayList<Book> books = new ArrayList<>();
        PreparedStatement statement = conn.prepareStatement("select * from book");
        ResultSet data = statement.executeQuery();

        List<Loan> borrowedBooks = borrowedBooksList();
        while (data.next()) {
            Book book = getBook(data.getInt("idBook"));
            if (borrowedBooks.stream().noneMatch(loan -> loan.book.idBook == book.idBook))
                books.add(book);
        }

        return books;
    }
}