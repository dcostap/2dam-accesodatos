package database;

public class Product {
	public int idProduct;
	public String name;
	public String description;
	public double price;
	public String picture;

	@Override
	public String toString() {
		return "Product{" +
				"idProduct=" + idProduct +
				", name='" + name + '\'' +
				", description='" + description + '\'' +
				", price=" + price +
				", picture='" + picture + '\'' +
				'}';
	}
}
