package database;

import java.sql.*;
import java.util.ArrayList;

import database.Client;
import database.Order;
import database.Product;

public class Operations {
	private Connection conn;

	public void openConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://192.168.56.101/orders";
			conn = DriverManager.getConnection(url, "manager", "abc123.");
		} catch (SQLException erro) {
			System.out.println("Erro SQL: " + erro.getMessage());
			erro.printStackTrace();
		} catch (Exception erro) {
			System.out.println("Erro: " + erro.getMessage());
			erro.printStackTrace();
		}
	}

	public void closeConnection() {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public boolean existsProduct(int id) {
		return getProduct(id) != null;
	}

	public Product getProduct(int id) {
		Product product = null;
		try {
			PreparedStatement statement = conn.prepareStatement("select * from product where idProduct like ?");

			statement.setString(1, Integer.toString(id));
			ResultSet data = statement.executeQuery();
			while (data.next()) {
				product = new Product();
				product.description = data.getString("description");
				product.name = data.getString("name");
				product.picture = data.getString("picture");
				product.price = data.getDouble("price");
				product.idProduct = data.getInt("idProduct");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return product;
	}

	public boolean existsClient(int id) {
		return getClient(id) != null;
	}

	public Client getClient(int id) {
		Client client = null;
		try {
			PreparedStatement statement = conn.prepareStatement("select * from client where idClient like ?");

			statement.setString(1, Integer.toString(id));
			ResultSet data = statement.executeQuery();
			while (data.next()) {
				client = new Client();
				client.name = data.getString("name");
				client.dni = data.getString("dni");
				client.idClient = data.getInt("idClient");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return client;
	}

	public Order getOrder(int id) {
		Order order = null;

		try {
			PreparedStatement statement = conn.prepareStatement("select * from orderr where idPedido like ?");

			statement.setString(1, Integer.toString(id));
			ResultSet data = statement.executeQuery();
			while (data.next()) {
				order = new Order();
				order.amount = data.getDouble("amount");
				order.client = getClient(data.getInt("idClient"));
				order.idPedido = data.getInt("idPedido");
				order.product = getProduct(data.getInt("idProduct"));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return order;
	}

	public int deleteOrder(int id) {
		try {
			PreparedStatement statement = conn.prepareStatement("delete from order where dni like ?");

			statement.setString(1, Integer.toString(id));
			statement.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return 0;
	}

	public int addOrder(Order order) {
		try {
			PreparedStatement statement = conn.prepareStatement("insert into orderr (idPedido, idProduct, idClient, amount) values (?, ?,?,?);");

			if (!existsClient(order.client.idClient))
				throw new RuntimeException("Client with id " + order.client.idClient + " doesn't exist in the database.");
			if (!existsProduct(order.product.idProduct))
				throw new RuntimeException("Product with id " + order.product.idProduct + " doesn't exist in the database.");

			statement.setString(1, String.valueOf(order.idPedido));
			statement.setString(2, String.valueOf(order.product.idProduct));
			statement.setString(3, String.valueOf(order.client.idClient));
			statement.setString(4, String.valueOf(order.amount));
			statement.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return 0;
	}

	public int modifyOrder(Order order) {
		try {
			PreparedStatement statement = conn
					.prepareStatement("update orderr set idProduct = ?, idClient = ?, amount = ? where idPedido like ?");

			if (!existsClient(order.client.idClient))
				System.out.println("Client with id " + order.client.idClient + " doesn't exist in the database.");
			else if (!existsProduct(order.product.idProduct))
				System.out.println("Product with id " + order.product.idProduct + " doesn't exist in the database.");
			else {
				statement.setString(1, String.valueOf(order.product.idProduct));
				statement.setString(2, String.valueOf(order.client.idClient));
				statement.setString(3, String.valueOf(order.amount));
				statement.setString(5, String.valueOf(order.idPedido));
				statement.executeUpdate();
				return 1;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return 0;
	}

	public ArrayList<Order> ordersList() {
		ArrayList<Order> orders = new ArrayList<>();
		try {
			PreparedStatement statement = conn.prepareStatement("select * from orderr");
			ResultSet data = statement.executeQuery();
			while (data.next()) {
				Order order = getOrder(data.getInt("idProduct"));
				orders.add(order);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return orders;
	}

}