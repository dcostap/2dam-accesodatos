package database;

public class Client {
	public int idClient;
	public String dni;
	public String name;

	@Override
	public String toString() {
		return "Client{" +
				"idClient=" + idClient +
				", dni='" + dni + '\'' +
				", name='" + name + '\'' +
				'}';
	}
}
