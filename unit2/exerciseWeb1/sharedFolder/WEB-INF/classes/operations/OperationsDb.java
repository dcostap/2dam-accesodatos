package operations;

import java.sql.*;
import java.util.ArrayList;

import operations.Student;

public class OperationsDb {
	private Connection conn;

	public void openConnection() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		String url = "jdbc:mysql://192.168.56.101/exercises";
		conn = DriverManager.getConnection(url, "manager", "abc123.");
	}

	public void closeConnection() throws SQLException {
		conn.close();
	}

	public void addStudent(Student student) throws SQLException {
		PreparedStatement statement = conn.prepareStatement("insert into student values (?,?,?,?)");

		statement.setString(1, student.getDni());
		statement.setString(2, student.getName());
		statement.setString(3, student.getSurname());
		statement.setInt(4, student.getAge());

		statement.executeUpdate();
	}

	public Student getStudent(String dni) throws SQLException {
		Student student = null;
		PreparedStatement statement = conn.prepareStatement("select * from student where dni like ?");

		statement.setString(1, dni);
		ResultSet data = statement.executeQuery();
		while (data.next()) {
			student = new Student(data.getString("dni"), data.getString("name"), data.getString("surname"),
					data.getInt("age"));
		}

		return student;
	}

	public void deleteStudent(String dni) throws SQLException {
		PreparedStatement statement = conn.prepareStatement("delete from student where dni like ?");

		statement.setString(1, dni);
		statement.executeUpdate();
	}

	public void modifyStudent(Student student) throws SQLException {
		PreparedStatement statement = conn
				.prepareStatement("update student set dni = ?, name = ?, surname = ?, age = ? where dni like ?");

		statement.setString(1, student.getDni());
		statement.setString(2, student.getName());
		statement.setString(3, student.getSurname());
		statement.setInt(4, student.getAge());
		statement.setString(5, student.getDni());
		statement.executeUpdate();
	}

	public ArrayList<Student> studentsList() throws SQLException {
		ArrayList<Student> students = new ArrayList<>();
		PreparedStatement statement = conn.prepareStatement("select * from student");
		ResultSet data = statement.executeQuery();
		while (data.next()) {
			Student student = new Student(data.getString("dni"), data.getString("name"), data.getString("surname"),
					data.getInt("age"));
			students.add(student);
		}

		return students;
	}

}