<%@page session="true" import="operations.*, java.util.*"%>
<html>

<head>
	<title>List of students</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<%
	String error = "";

	if (session.getAttribute("error") != null)
		error += session.getAttribute("error") + "\n";
	ArrayList<Student> students = new ArrayList<Student>();
	try {
		OperationsDb ops = new OperationsDb();
		ops.openConnection();
		students = ops.studentsList();
		} catch (Exception e) {
			error += e.getMessage();
		}
%>

<script>

	function openForm() {
		document.getElementById("form-popup").style.display = "block";
		document.getElementById("overlay").style.display = "block";
		document.getElementById("form").reset();
	}

	function closeForm() {
		document.getElementById("form-popup").style.display = "none";
		document.getElementById("overlay").style.display = "none";
		document.getElementById("form").reset();
	}
</script>

<body>
	<div id="overlay"></div>

	<div class="table-div" style="max-width: 700px; margin: 0 auto;">
		<table border="2">
			<tr class="bold table-head">
				<td>
					
				</td>
			</tr>
			<%
		for (Student s : students) {
	%>
			<tr class="table_back1">
				<td>
					<%= s.getDni() %>
				</td>
				<td>
					<%= s.getName() %>
				</td>
				<td>
					<%= s.getSurname() %>
				</td>
				<td>
					<%= s.getAge() %>
				</td>
				<td style="width: 120px;">
					<span>
						<a href="<%= "modifyStudentForm.jsp?dni=" + s.getDni() %>">Modify</a>
					</span>
					<span> | </span>
					<span>
						<a href="<%= "deleteStudent.jsp?dni=" + s.getDni() %>">Delete</a>
					</span>

				</td>
			</tr>
			<%
		}
	%>
		</table>
	</div>

	<br />
	<div class="centered" style="margin: 0 auto;">
		<!-- <button onclick="window.location.href='studentsInputForm.html'">Add a student</button> -->
		<button class="button" onclick="openForm()">Add a student</button>
	</div>

	<div class="errors">
		<%
		if (!error.isEmpty())
			out.println("Error: " + error);

		session.invalidate();
		%>
	</div>

	<div id="form-popup">
		<h2 class="centered">Input the info to create a new student</h2>

		<form action="addStudent.jsp" id="form" method="get">
			<div>
				<label for="dni">DNI</label>
				<input type="text" id="dni" name="vDni" placeholder="Dni" required>

				<br />

				<label for="name">Name</label>
				<input type="text" id="name" name="vName" placeholder="Name" required>

				<br />

				<label for="surname">Surname</label>
				<input type="text" id="surname" name="vSurname" placeholder="Surname" required>

				<br />

				<label for="age">Age</label>
				<input type="number" min="0" max="150" id="age" name="vAge" placeholder="Age" required>

				<br />
				<br />

				<div class="centered">
					<input class="cancel button" type="button" value="Cancel" onclick="closeForm()"
						style="margin-right: 20px;" />
					<input class="button" type="submit" value="Create" />
				</div>
			</div>
		</form>
	</div>
</body>

</html>