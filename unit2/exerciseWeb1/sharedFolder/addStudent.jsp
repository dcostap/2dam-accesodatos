<%@page session="true" import="operations.*, java.util.*"%>
<html>

<head>
	<title>Adding a student</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<%
try {
	OperationsDb ops = new OperationsDb();
	ops.openConnection();

	Student st = new Student(
		request.getParameter("vDni"),
		request.getParameter("vName"),
		request.getParameter("vSurname"),
		Integer.parseInt(request.getParameter("vAge"))
	);

	ops.addStudent(st);

	ops.closeConnection();
} catch (Exception e) {
	session.setAttribute("error", e.getMessage());
}

	response.sendRedirect("studentsList.jsp");
%>

<body>

</body>

</html>