<%@page import="operations.*, java.util.*"%>

<html>

<head>
	<title>Add student</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<%
	OperationsDb ops = new OperationsDb();
	ops.openConnection();

	if (request.getParameter("dni") != null) {
		Student st = ops.getStudent(request.getParameter("dni"));
	%>

	<div style="max-width: 400px; margin: 0 auto;">
		<h1 class="centered">
			Modifying student:
			<%= st.getName() + " " + st.getSurname() %>
		</h1>
		<form action="modifyStudent.jsp" method="get">
			<div>
				<label for="dni">DNI</label>
				<input type="text" id="dni" name="vDni" value="<%= st.getDni() %>" readonly="readonly">

				<br />

				<label for="name">Name</label>
				<input type="text" id="name" name="vName" value="<%= st.getName() %>" required>

				<br />

				<label for="surname">Surname</label>
				<input type="text" id="surname" name="vSurname" value="<%= st.getSurname() %>" required>

				<br />

				<label for="age">Age</label>
				<input type="number" min="0" max="150" id="age" name="vAge" value="<%= st.getAge() %>" required>

				<br />

				<div class="centered" style="max-width: 500px; margin: 0 auto;">
					<input class="button" type="submit" value="Modify" style="margin-bottom: 9px;" />
					<input class="button cancel" type="button" value="Cancel"
						onclick="window.location.href='studentsList.jsp'"/>
			</div>
		</form>
	</div>

	<% } else {
		response.sendRedirect("studentsList.jsp");
	} %>

</body>

</html>