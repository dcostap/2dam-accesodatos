<%@page import="operations.*, java.util.*"%>
<html>

<head>
	<title>Deleting a student</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<%
	try {
		OperationsDb ops = new OperationsDb();
		ops.openConnection();

		ops.deleteStudent(request.getParameter("dni"));

		ops.closeConnection();
	} catch (Exception e) {
		session.setAttribute("error", e.getMessage());
	}

		response.sendRedirect("studentsList.jsp");
%>

<body>

</body>

</html>