import operations.OperationsDb;
import operations.Student;

import java.util.Scanner;

public class Program {
	public static void main(String[] args) {
		OperationsDb operations = new OperationsDb();
		operations.openConnection();

		operations.addStudent(new Student("dni", "name", "surname", 123));

		System.out.println("List of students:");
		for (Student s : operations.studentsList()) {
			System.out.println(s.toString());
		}

		System.out.println();
		System.out.println();

		Scanner sc = new Scanner(System.in);

		System.out.println("Adding a new user...");
		System.out.println("Type the DNI...");
		String dni = sc.nextLine();
		System.out.println("Type the name...");
		String name = sc.nextLine();
		System.out.println("Type the surname...");
		String surname = sc.nextLine();
		System.out.println("Type the age...");
		int age = sc.nextInt();

		Student newStudent = new Student(dni, name, surname, age);
		operations.addStudent(newStudent);

		System.out.println("Showing data of a user:");
		System.out.println("Type the user dni...");
		sc.nextLine();
		String retrieveDni = sc.nextLine();
		Student resultStudent = operations.getStudent(retrieveDni);
		if (resultStudent == null) {
			System.out.println("Didn't find any user with that DNI!");
		} else {
			System.out.println(resultStudent.toString());
		}

		System.out.println("Deleting a user:");
		System.out.println("Type the user dni...");
		String deleteDni = sc.nextLine();
		operations.deleteStudent(retrieveDni);

		System.out.println("Modifying a user:");
		System.out.println("Type the dni of the user we will modify...");
		String modifyDniOrig = sc.nextLine();

		System.out.println("Creating the new data:");
		System.out.println("Type the name...");
		String nameNew = sc.nextLine();
		System.out.println("Type the surname...");
		String surnameNew = sc.nextLine();
		System.out.println("Type the age...");
		int ageNew = sc.nextInt();

		Student newModifiedStudent = new Student(modifyDniOrig, nameNew, surnameNew, ageNew);

		Student check = operations.getStudent(modifyDniOrig);
		if (check == null) {
			System.out.println("Didn't find any user with that DNI!");
		} else {
			operations.modifyStudent(newModifiedStudent);
		}

		System.out.println("How the table looks now:");
		for (Student s : operations.studentsList()) {
			System.out.println(s.toString());
		}

		operations.closeConnection();
	}
}