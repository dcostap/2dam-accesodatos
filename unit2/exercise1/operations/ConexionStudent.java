package operations;

import java.sql.*;

public class ConexionStudent {
	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://192.168.56.101/exercises";
			Connection conexion = DriverManager.getConnection(url, "manager", "abc123.");

			DatabaseMetaData meta = conexion.getMetaData();
			System.out.println("A version do driver JDBC e: " + meta.getDriverVersion());

			Statement consulta = conexion.createStatement();
			ResultSet datos = consulta.executeQuery("select * from student");
			while (datos.next()) {
				String dni = datos.getString("dni");
				String nome = datos.getString("name");
				String apelidos = datos.getString("surname");
				int idade = datos.getInt("age");
				System.out.println(dni);
			}

			conexion.close();
		} catch (SQLException erro) {
			System.out.println("Erro SQL: " + erro.getMessage());
			erro.printStackTrace();
		} catch (Exception erro) {
			System.out.println("Erro: " + erro.getMessage());
			erro.printStackTrace();
		}
	}
}