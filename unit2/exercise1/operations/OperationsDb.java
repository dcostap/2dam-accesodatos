package operations;

import java.sql.*;
import java.util.ArrayList;

import operations.Student;

public class OperationsDb {
	private Connection conn;

	public void openConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://192.168.56.101/exercises";
			conn = DriverManager.getConnection(url, "manager", "abc123.");
		} catch (Exception error) {
			error.printStackTrace();
		}
	}

	public void closeConnection() {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void addStudent(Student student) {
		try {
			PreparedStatement statement = conn.prepareStatement("insert into student values (?,?,?,?)");

			statement.setString(1, student.getDni());
			statement.setString(2, student.getName());
			statement.setString(3, student.getSurname());
			statement.setInt(4, student.getAge());

			statement.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public Student getStudent(String dni) {
		Student student = null;
		try {
			PreparedStatement statement = conn.prepareStatement("select * from student where dni like ?");

			statement.setString(1, dni);
			ResultSet data = statement.executeQuery();
			while (data.next()) {
				student = new Student(data.getString("dni"), data.getString("name"), data.getString("surname"),
						data.getInt("age"));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return student;
	}

	public int deleteStudent(String dni) {
		try {
			PreparedStatement statement = conn.prepareStatement("delete from student where dni like ?");

			statement.setString(1, dni);
			statement.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return 0;
	}

	public int modifyStudent(Student student) {
		try {
			PreparedStatement statement = conn
					.prepareStatement("update student set dni = ?, name = ?, surname = ?, age = ? where dni like ?");

			statement.setString(1, student.getDni());
			statement.setString(2, student.getName());
			statement.setString(3, student.getSurname());
			statement.setInt(4, student.getAge());
			statement.setString(5, student.getDni());
			statement.executeUpdate();
			return 1;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return 0;
	}

	public ArrayList<Student> studentsList() {
		ArrayList<Student> students = new ArrayList<>();
		try {
			PreparedStatement statement = conn.prepareStatement("select * from student");
			ResultSet data = statement.executeQuery();
			while (data.next()) {
				Student student = new Student(data.getString("dni"), data.getString("name"), data.getString("surname"),
						data.getInt("age"));
				students.add(student);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return students;
	}

}