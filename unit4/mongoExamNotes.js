// NOTAS ////////////////////
$project = map en prog.funcional, é como un println

aggregate allows for arbitrary number of operations
find is like a comfy aggregate with one match and project

find = match + project


collection = table
document = table row

////////////////////

use miBD; // creates the database

db.equipos.drop()
db.persoas.drop()
db.createCollection("equipos");
db.createCollection("persoas");

xogador1 = {
    dni: '77757055L',
    nomeApelidos: 'Manolo Fernández Pérez',
    anoNacemento: '1990',
    telefono: 889500165,
    enderezo: {
        rua: 'Lugar Arcai de Abaixo',
        cp: '15688'
    }
}

equipo2 = {
    nome: 'Tordoia FC',
    entrenador: entrenador1,
    xogadores: [
        xogador2, xogador3
    ],
    lugar: 'Tordoia'
}

pedido1 = {
    data: new Date(),
    cliente: "Pepe Martínez",
    lineasPedido: [],
    importe: 100.2,
    entregado: true
}

db.persoas.insert(xogador1);

db.persoas.find().pretty();

// busca o equipo con un xogador que ten un dni en concreto
db.persoas.find(
    { "xogadores.dni": "whatever" }
)

db.empregados.find().sort(
    { salario: -1 }
)

db.pedidos.find(
    {
        "lineasPedido": {
            $elemMatch: { "cantidade": { $gt: 1 } } // elemMatch busca dentro de un array si algún valor coincide
        }
    }
).pretty()

// enseña nomes dos empregados nos departamentos 10 e 20
db.empregados.find({
    $or: [
        { departamento: 10 },
        { departamento: 20 }
    ]
}, {
    nome: 1 // devolve só o campo nome
})

// Os nomes e oficios dos empregados con salario > 1400 e oficio “Analista”
db.empregados.find({
    $and: [
        { salario: { $gt: 1400 } },
        { oficio: "Analista" }
    ]
}, {
    nome: 1,
    oficio: 1,
})

// Os nomes e oficios dos empregados con oficio na lista de valores: [“Programador”,
// “Técnico”, “Analista”] e número de departamento igual ou maior cá 20.
db.empregados.find({
    $and: [
        {
            oficio: {
                $in: [
                    "Programador",
                    "Técnico",
                    "Analista"
                ]
            }
        },
        {
            departamento: {
                $gte: 20
            }
        }
    ]
}, {
    nome: 1,
    oficio: 1,
})

// modifica lugar do equipo con nome tordoia
db.equipos.update({ "nome": "Tordoia FC" }, { $set: { "lugar": "NovoLugar" } })

// modifica teléfono do xogador con un dni concreto, en todos os documentos no que apareza
db.equipos.updateMany({ "xogadores.dni": "45957088J" }, { $set: { "xogadores.$.telefono": 1111111 } })

// sum importes of pedidos with entregado = true
db.pedidos.aggregate([
    {
        $group: {
            _id: null,
            total: {
                "$sum": {
                    "$cond": [
                        "$entregado", // boolean
                        "$importe", // si se cumple
                        0 // si non se cumple
                    ]
                }
            }
        }
    }]
)

// engade ao arrai lineasPedido de un pedido en concreto
db.pedidos.update(
    { "importe": 33.5 },
    { $push: { lineasPedido: lineaPedido1 } }
)

db.pedidos.update(
    { "importe": 33.5 },
    { $pop: { lineasPedido: 1 } }
)

// engadir 100 de salario a todos
db.empregados.updateMany(
    {},
    {
        $inc: { "salario": 100 }
    }
)

// Decrementa a comisión en 20 euros, só ós que teñan comisión.
db.empregados.updateMany(
    { "comisión": { $exists: true } },
    {
        $inc: { "comisión": -20 }
    }
)

// Borra o empregado con empNo=2
db.empregados.deleteOne(
    { empNo: 2 }
)

// Crea unha nova colección de nome empre que conteña unicamente o nome e o oficio de
// cada empregado, co oficio en minúsculas. Os empregados deben estar ordenados por nome
// na nova colección.
db.empregados.aggregate([
    {
        $sort: {
            nome: 1
        }
    },
    {
        $project: { // qué campos amosas
            nome: '$nome',
            oficio: { $toLower: '$oficio' }
        }
    },
    {
        $out: "empre" // crea nova colección de nome "empre" co output
    }
]);

// Mostrar por pantalla os nomes e oficios dos empregados cuxo nome empece por “A”.
db.empregados.aggregate(
    [
        {
            $match: {
                nome: /^A/
            }
        },
        {
            $group: {
                _id: null, // id = null means it won't group, so this runs for every entry
                salarios: { $sum: '$salario' }
            }
        }
    ]
)


// Obter para cada departamento, o número de empregados que hai, o salario máis alto e o
// nome do traballador co salario máis alto.

// para sacar o nome do traballador co salario máis alto,
// primeiro sorteamos toda a info segundo o salario
// despois pillamos o primeiro elemento da lista
db.empregados.aggregate(
    [
        {
            $sort: {
                salario: -1
            }
        },
        {
            $group: {
                "_id": "$departamento",
                "númeroEmpregados": { $sum: 1 },
                "salarioMáisAlto": { $max: "$salario" },
                "nomeTraballadorSalarioMáisAlto": {
                    $first: "$nome"
                }
            }
        }
    ]
)


db.matches.aggregate([
    {
        $match: {
            'team1.name': 'Beluso'
        }
    },
    {
        $group: {
            _id: '$team1.id', // non null id prob. means that group will separate all items into groups by this id, and it will iterate over each group separately
                                // in this case since team1 is always the same due to the previous match, this means the result is 1 thing
            teamName: { $first: '$team1.name' }, // picks the value on the first iteration
            totalPoints: { $sum: '$team1.points' } // keeps accumulating on each iteration
        }
    },
    {
        $out: 'cpoints'
    }
])