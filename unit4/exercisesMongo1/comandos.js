use liga;

db.equipos.drop()
db.persoas.drop()
db.createCollection("equipos");

db.createCollection("persoas");

xogador1 = {
    dni: '77757055L',
    nomeApelidos: 'Manolo Fernández Pérez',
    anoNacemento: '1990',
    telefono: 889500165,
    enderezo: {
        rua: 'Lugar Arcai de Abaixo',
        cp: '15688'
    }
}

xogador2 = {
    dni: '45957088J',
    nomeApelidos: 'Adrián Pérez Gómez',
    anoNacemento: '2001',
    telefono: 609500165,
    enderezo: {
        rua: 'Rúa Rosalía de Castro nº 20',
        cp: '15888'
    }
}

xogador3 = {
    dni: '11157055L',
    nomeApelidos: 'Antonio González Pérez',
    anoNacemento: '1988',
    telefono: 889500888,
    enderezo: {
        rua: 'Lugar Tordoia',
        cp: '15688'
    }
}

entrenador1 = {
    dni: '45957999L',
    nomeApelidos: 'Andrés Costa Pérez',
    anoNacemento: '1970',
    telefono: 609500777,
    enderezo: {
        rua: 'Lugar Tordoia',
        cp: '15688'
    }
}

entrenador2 = {
    dni: '45957055L',
    nomeApelidos: 'Pepe García Pérez',
    anoNacemento: '1990',
    telefono: 609500165,
    enderezo: {
        rua: 'Lugar Arcai de Arriba',
        cp: '15677'
    }
}

equipo1 = {
    nome: 'Ordes FC',
    entrenador: entrenador2,
    xogadores: [,
        xogador1, xogador2
    ],
    lugar: 'Ordes'
}

equipo2 = {
    nome: 'Tordoia FC',
    entrenador: entrenador1,
    xogadores: [
        xogador2, xogador3
    ],
    lugar: 'Tordoia'
}

db.persoas.insert(xogador1);
db.persoas.insert(xogador2);
db.persoas.insert(xogador3);
db.persoas.insert(entrenador1);
db.persoas.insert(entrenador2);
db.equipos.insert(equipo1);
db.equipos.insert(equipo2);

db.equipos.update({ "nome": "Tordoia FC" }, { $set: { "lugar": "NovoLugar" } })
db.equipos.updateMany({ "xogadores.dni": "45957088J" }, { $set: { "xogadores.$.telefono": 1111111 } })


// exercicio pedidos

use pedidos;
db.pedidos.drop()
db.createCollection("pedidos")

pedido1 = {
    data: new Date(),
    cliente: "Pepe Martínez",
    lineasPedido: [],
    importe: 100.2,
    entregado: true
}

pedido2 = {
    data: new Date(),
    cliente: "Manolo Costa",
    lineasPedido: [],
    importe: 33.5,
    entregado: false
}

db.pedidos.insert(pedido1)
db.pedidos.insert(pedido2)

// sum importes of pedidos with entregado = true
db.pedidos.aggregate([
    {
        $group: {
            _id: null,
            total: {
                "$sum": {
                    "$cond": [
                        "$entregado",
                        "$importe",
                        0
                    ]
                }
            }
        }
    }]
)

lineaPedido1 = {
    produto: {
        nomeProduto: "Produto1",
        descricion: "Produto1 Descrición",
        foto: "Produto1 Foto",
        prezo: 33.0,
    },
    cantidade: 2,
    prezoTotal: 33.0,
}

lineaPedido2 = {
    produto: {
        nomeProduto: "Produto1",
        descricion: "Produto1 Descrición",
        foto: "Produto1 Foto",
        prezo: 33.0,
    },
    cantidade: 1,
    prezoTotal: 33.0,
}

db.pedidos.update(
    { "importe": 33.5 },
    { $push: {lineasPedido: lineaPedido1 }}
)

db.pedidos.update(
    { "importe": 100.2 },
    { $push: {lineasPedido: lineaPedido2 }}
)

db.pedidos.update(
    { "importe": 33.5 },
    { $pop: {lineasPedido: 1 }}
)

db.pedidos.find(
    {
        "lineasPedido": {
            $elemMatch: { "cantidade": { $gt: 1 }}
        }
    }
).pretty()


// notas random
$project = map en prog. funcional, é como un println

find = match + project