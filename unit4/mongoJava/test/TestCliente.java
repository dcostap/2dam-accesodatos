package test;

import java.util.ArrayList;

import org.bson.Document;

import baseDatos.Cliente;
import baseDatos.Enderezo;
import baseDatos.Operacions;

public class TestCliente {
    public static void main(String[] args) {
        try {
            Operacions op = new Operacions();
            op.abrirConexion("biblioteca");

            System.out.println("___________");
            System.out.println("Creamos 2 clientes:");

            Cliente cliente1 = new Cliente("55667799Y", "Pepe", "González",
                    new Document[] {
                        new Enderezo("Rúa Rosalía de Castro", 10, "Ordes", "A Coruña").generateDocument()
                    });

            Cliente cliente2 = new Cliente("99667799Y", "María", "Costa",
                    new Document[] {
                        new Enderezo("Rúa Avenida", 7, "Santiago", "A Coruña").generateDocument()
                    });

            System.out.println(cliente1);
            System.out.println(cliente2);


            System.out.println("___________");
            System.out.println("Engadimos os clientes á base de datos...");

            try {
                op.creaColeccion("clientes");
            } catch (Exception e) {
                System.out.println("A colección 'clientes' xa existía, polo que non se volverá a crear.");
            }

            op.engadeDocumento(cliente1.generateDocument(), "clientes");
            op.engadeDocumento(cliente2.generateDocument(), "clientes");

            System.out.println("___________");
            System.out.println("Lendo na base datos o que acabamos de engadir:");
            op.listado("clientes").forEach(System.out::println);

            op.cerrarConexion();
        } catch (Exception e) {
            System.out.println("Houbo unha excepción: " + e.getMessage());
            System.out.println("Mensaxe completo:");
            e.printStackTrace();
        }
    }
}
