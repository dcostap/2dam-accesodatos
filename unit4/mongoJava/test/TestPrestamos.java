package test;

import java.util.ArrayList;
import java.util.Date;

import org.bson.Document;

import baseDatos.Cliente;
import baseDatos.Enderezo;
import baseDatos.Libro;
import baseDatos.Operacions;
import baseDatos.Prestamo;

public class TestPrestamos {
    public static void main(String[] args) {
        try {
            Operacions op = new Operacions();
            op.abrirConexion("biblioteca");

            System.out.println("___________");
            System.out.println("Creamos 2 clientes:");

            Cliente cliente1 = new Cliente("55667799Y", "Pepe", "González",
                    new Document[] {
                        new Enderezo("Rúa Rosalía de Castro", 10, "Ordes", "A Coruña").generateDocument()
                    });

            Cliente cliente2 = new Cliente("99667799Y", "María", "Costa",
                    new Document[] {
                        new Enderezo("Rúa Avenida", 7, "Santiago", "A Coruña").generateDocument()
                    });

            System.out.println(cliente1);
            System.out.println(cliente2);

            System.out.println("___________");
            System.out.println("Creamos 2 libros:");

            Libro libro1 = new Libro(
                "978-3-16-148410", "Woodstock Handmade Houses", new String[] {
                    "Ballantine David",
                    "Jonathan Elliott"
                });

            Libro libro2 = new Libro(
                "0-7475-3849-2", "Harry Potter and the Chamber of Secrets", new String[] {
                    "J.K. Rowling"
                });

            System.out.println(libro1);
            System.out.println(libro2);

            System.out.println("___________");
            System.out.println("Creamos 2 préstamos:");

            Prestamo prestamo1 = new Prestamo(new Date(), cliente1.generateDocument(), libro1.generateDocument());
            Prestamo prestamo2 = new Prestamo(new Date(), cliente2.generateDocument(), libro2.generateDocument());

            System.out.println(prestamo1);
            System.out.println(prestamo2);

            System.out.println("___________");
            System.out.println("Engadimos os préstamos á base de datos...");

            try {
                op.creaColeccion("prestamos");
            } catch (Exception e) {
                System.out.println("A colección 'prestamos' xa existía, polo que non se volverá a crear.");
            }

            op.engadeDocumento(prestamo1.generateDocument(), "prestamos");
            op.engadeDocumento(prestamo2.generateDocument(), "prestamos");

            System.out.println("___________");
            System.out.println("Lendo na base datos o que acabamos de engadir:");
            op.listado("prestamos").forEach(System.out::println);

            op.cerrarConexion();
        } catch (Exception e) {
            System.out.println("Houbo unha excepción: " + e.getMessage());
            System.out.println("Mensaxe completo:");
            e.printStackTrace();
        }
    }
}
