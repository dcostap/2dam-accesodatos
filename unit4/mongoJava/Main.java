
import com.mongodb.*;
import com.mongodb.client.*;
import com.mongodb.client.model.Sorts;
import org.bson.Document;

import baseDatos.Operacions;

import static com.mongodb.client.model.Filters.*;
import com.mongodb.client.result.*;
import static com.mongodb.client.model.Updates.*;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        try {
			Operacions op = new Operacions();
			op.abrirConexion("biblioteca");

			op.cerrarConexion();

            //Engadir documento
            Document unDocumento = new Document()
            		.append("titulo", "Exemplo")
            		.append("valor", 30);
            //db.getCollection("coleccionExemplo").insertOne(unDocumento);

            //Engadir documento con info dun grupo de alumnado cun campo tipo array
            ArrayList<Document> alumnos = new ArrayList<Document>();
            Document documento1= new Document()
					.append("dni","11111111A")
					.append("nome","Xosé")
					.append("apelidos","Agulla Ferradás")
					.append("idade", 20);
            alumnos.add(documento1);
            Document documento2= new Document()
					.append("dni","22222222B")
					.append("nome","Carmen")
					.append("apelidos","Martínez")
					.append("idade", 22);
			alumnos.add(documento2);
			//Grupo de alumnos
			Document documento3 = new Document()
					.append("id", 13)
					.append("Grupo", "2 DAM")
					.append("rexime", "ordinario")
					.append("alumnos", alumnos);
			//db.getCollection("coleccionExemplo").insertOne(documento3);
			documento3 = new Document()
					.append("id", 10)
					.append("Grupo", "2 DAM")
					.append("rexime", "modular");
			//db.getCollection("coleccionExemplo").insertOne(documento3);
			documento3 = new Document()
					.append("id", 20)
					.append("Grupo", "2 DAM")
					.append("rexime", "distancia");
			//db.getCollection("coleccionExemplo").insertOne(documento3);

			// //Listado da coleccion
			// ArrayList<Document> listado = db.getCollection("coleccionExemplo").find().into(new ArrayList<Document>());
			// System.out.println("LISTADO:");
			// for (Document documento:listado)
			// 	System.out.println(documento);

			// //Listado da coleccion ordenado
			// listado = db.getCollection("coleccionExemplo").find().sort(Sorts.descending("id")).into(new ArrayList<Document>());
			// System.out.println("LISTADO:");
			// for (Document documento:listado)
			// 	System.out.println(documento);

			// //Modificar documento
			// UpdateResult resultado = db.getCollection("coleccionExemplo").updateOne(eq("id",20),combine(set("Grupo","2 ASIR")));
			// System.out.println("Número de documentos modificados: "+resultado.getModifiedCount());

			// //Busca con condicion
			// Document documento4 = db.getCollection("coleccionExemplo").find(eq("id",20)).first();
			// System.out.println("Documento buscado: "+documento4);

            // //Cerrar conexion
            // mongoClient.close();
        } catch (Exception erro) {
            System.out.println("Erro no main: "+erro.getMessage());
        }
	}

}