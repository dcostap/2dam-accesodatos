package baseDatos;

import org.bson.Document;

public interface IMongoDocument {
    Document generateDocument();
}
