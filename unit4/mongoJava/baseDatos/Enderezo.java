package baseDatos;

import org.bson.Document;

public class Enderezo implements IMongoDocument {
    private String rua;
    private int num;
    private String localidade;
    private String provincia;

    public Enderezo(String rua, int num, String localidade, String provincia) {
        this.rua = rua;
        this.num = num;
        this.localidade = localidade;
        this.provincia = provincia;
    }

    @Override
    public Document generateDocument() {
        Document doc = new Document()
            .append("rúa", rua)
            .append("número", num)
            .append("localidade", localidade)
            .append("provincia", provincia);

        return doc;
    }
}
