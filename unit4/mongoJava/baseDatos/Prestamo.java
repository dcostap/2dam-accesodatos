package baseDatos;

import java.util.Date;

import org.bson.Document;

public class Prestamo implements IMongoDocument {
    private Date data;
    private Document cliente;
    private Document libro;

    public Prestamo(Date data, Document cliente, Document libro) {
        this.data = data;
        this.cliente = cliente;
        this.libro = libro;
    }

    @Override
    public Document generateDocument() {
        Document doc = new Document()
            .append("data", data.toString())
            .append("cliente", cliente)
            .append("libro", libro);

        return doc;
    }
}
