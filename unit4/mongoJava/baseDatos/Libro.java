package baseDatos;

import java.util.Arrays;

import org.bson.Document;

public class Libro implements IMongoDocument {
    private String isbn;
    private String titulo;
    private String[] autores;

    public Libro(String isbn, String titulo, String[] autores) {
        this.isbn = isbn;
        this.titulo = titulo;
        this.autores = autores;
    }

    @Override
    public Document generateDocument() {
        Document doc = new Document()
            .append("isbn", isbn)
            .append("titulo", titulo)
            .append("autores", Arrays.asList(autores));

        return doc;
    }
}
