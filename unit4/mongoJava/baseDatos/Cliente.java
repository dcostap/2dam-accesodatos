package baseDatos;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import org.bson.Document;

public class Cliente implements IMongoDocument {
    private String dni;
    private String nome;
    private String apelidos;
    private Document[] enderezos;

    public Cliente(String dni, String nome, String apelidos, Document[] enderezos) {
        this.dni = dni;
        this.nome = nome;
        this.apelidos = apelidos;
        this.enderezos = enderezos;
    }

    @Override
    public Document generateDocument() {
        Document doc = new Document()
            .append("rúa", dni)
            .append("nome", nome)
            .append("apelidos", apelidos)
            .append("enderezos", Arrays.asList(enderezos));

        return doc;
    }

    @Override
    public String toString() {
        return "Cliente [apelidos=" + apelidos + ", dni=" + dni + ", enderezos=" + Arrays.toString(enderezos)
                + ", nome=" + nome + "]";
    }
}
