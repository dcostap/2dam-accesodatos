package baseDatos;

import com.mongodb.*;
import com.mongodb.client.*;
import com.mongodb.client.model.Sorts;
import org.bson.Document;
import static com.mongodb.client.model.Filters.*;
import com.mongodb.client.result.*;
import static com.mongodb.client.model.Updates.*;
import java.util.ArrayList;

public class Operacions {
	private MongoClient mongoCliente;
	private MongoDatabase db;

	public void abrirConexion(String nomeBD) {
		mongoCliente= new MongoClient("192.168.56.102");
		db= mongoCliente.getDatabase(nomeBD);
	}

	public void cerrarConexion() throws Exception{
		mongoCliente.close();
	}

	//Metodo "creaColeccion" que recibe o nome dunha coleccion e a crea
	public void creaColeccion(String nomeColeccion) throws Exception {
        db.createCollection(nomeColeccion);
	}

	//Metodo "engadeDocumento" que recibe un documento e un nome de coleccion
	//Engade o documento na coleccion
	public void engadeDocumento(Document doc, String nomeColeccion) throws Exception{
		db.getCollection(nomeColeccion).insertOne(doc);
	}

	//Metodo "listado" que devolve listado de documentos e recibe o nome da colección
	public ArrayList<Document> listado(String nomeColeccion){
		ArrayList<Document> lista = db.getCollection(nomeColeccion).find().into(new ArrayList<Document>());
		return lista;
	}

	//Metodo "consultaDocumento"
	//Recibe: nome da coleccion, campo de busca, valor de busca
	//Devolve: Document
	public Document consultaDocumento(String nomeColeccion, String campo, Object valor) {
		Document documento = db.getCollection(nomeColeccion).find(eq(campo,valor)).first();
		return documento;
	}

	//Metodo "modifica"
	//Recibe: nome da coleccion, campo de busca, valor de busca, campo a modificar, valor a asignar
	public long modifica(String nomeColeccion, String campoConsulta, Object valorConsulta, String campoModificar, Object valorModificar) {
		 UpdateResult resultado = db.getCollection(nomeColeccion).updateOne(eq(campoConsulta,valorConsulta),combine(set(campoModificar,valorModificar)));
		 return resultado.getModifiedCount();
	}

	//Metodo "borraDocumento"
	//Recibe: nome da coleccion, campo de busca, valor de busca
	public long borraDocumento(String nomeColeccion, String campo, Object valor) throws Exception{
		DeleteResult resultado=db.getCollection(nomeColeccion).deleteMany(eq(campo,valor));
		return resultado.getDeletedCount();
	}

}//Operacions