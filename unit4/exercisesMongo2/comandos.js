use miBD;

db.empregados.drop()
db.createCollection("empregados");

emp1 = {
    empNo: 1,
    nome: 'Juan',
    departamento: 10,
    salario: 1000,
    dataAlta: "10/10/1999",
    oficio: "Técnico"
}

emp2 = {
    empNo: 2,
    nome: 'Alicia',
    departamento: 10,
    salario: 1400,
    dataAlta: "07/08/2000",
    oficio: "Profesora"
}

emp3 = {
    empNo: 3,
    nome: 'María Jesús',
    departamento: 20,
    salario: 1500,
    dataAlta: "05/01/2005",
    oficio: "Analista",
    comisión: 100
}

emp4 = {
    empNo: 4,
    nome: 'Alberto',
    departamento: 20,
    salario: 1100,
    dataAlta: "15/11/2001",
    oficio: "Técnico"
}

emp5 = {
    empNo: 5,
    nome: 'Fernando',
    departamento: 30,
    salario: 1300,
    dataAlta: "20/11/1999",
    oficio: "Analista",
    comisión: 200
}

db.empregados.insert(emp1);
db.empregados.insert(emp2);
db.empregados.insert(emp3);
db.empregados.insert(emp4);
db.empregados.insert(emp5);

db.empregados.find().sort(
    { salario: -1 }
)

db.empregados.find({
    $or: [
        { departamento: 10 },
        { departamento: 20 }
    ]
}, {
    nome: 1 // devolve só o campo nome
})

db.empregados.find({
    $and: [
        { salario: { $gt: 1400 } },
        { oficio: "Analista" }
    ]
}, {
    nome: 1,
    oficio: 1,
})

db.empregados.find({
    $and: [
        {
            oficio: {
                $in: [
                    "Programador",
                    "Técnico",
                    "Analista"
                ]
            }
        },
        {
            departamento: {
                $gte: 20
            }
        }
    ]
}, {
    nome: 1,
    oficio: 1,
})

// engadir 100 de salario a todos
db.empregados.updateMany(
    {},
    {
        $inc: { "salario": 100 }
    }
)

db.empregados.updateMany(
    { "comisión": { $exists: true } },
    {
        $inc: { "comisión": -20 }
    }
)

db.empregados.deleteOne(
    { empNo: 2 }
)

db.empregados.aggregate([
    {
        $sort: {
            nome: 1
        }
    },
    {
        $project: {
            nome: '$nome',
            oficio: { $toLower: '$oficio' }
        }
    },
    {
        $out: "empre"
    }
]);

db.empregados.aggregate(
    [
        {
            $match: {
                nome: /^A/ // só nomes que empezan por A
            }
        },
        {
            $group: {
                _id: null,
                salarios: { $sum: '$salario' }
            }
        }
    ]
)


// para sacar o nome do traballador co salario máis alto,
// primeiro sorteamos toda a info segundo o salario
// despois pillamos o primeiro elemento da lista
db.empregados.aggregate(
    [
        {
            $sort: {
                salario: -1
            }
        },
        {
            $group: {
                "_id": "$departamento",
                "númeroEmpregados": { $sum: 1 },
                "salarioMáisAlto": { $max: "$salario" },
                "nomeTraballadorSalarioMáisAlto": {
                    $first: "$nome"
                }
            }
        }
    ]
)